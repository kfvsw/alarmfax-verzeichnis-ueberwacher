﻿/*
    Copyright (C) 2020 Ralf Weippert - All Rights Reserved    

    This file is part of Alarmfax-Verzeichnis-Überwacher.

    Alarmfax-Verzeichnis-Überwacher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Alarmfax-Verzeichnis-Überwacher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alarmfax-Verzeichnis-Überwacher.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Alarmfax-Verzeichnis-Überwacher.

    Das Programm Alarmfax-Verzeichnis-Überwacher ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Das Programm Alarmfax-Verzeichnis-Überwacher wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

namespace WindowsFormsApplication1
{
    partial class FormMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labStatus = new System.Windows.Forms.Label();
            this.cmdStart = new System.Windows.Forms.Button();
            this.cmdStop = new System.Windows.Forms.Button();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuHauptmenü = new System.Windows.Forms.MenuStrip();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuHauptmenü.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // labStatus
            // 
            this.labStatus.AutoSize = true;
            this.labStatus.Location = new System.Drawing.Point(448, 125);
            this.labStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labStatus.Name = "labStatus";
            this.labStatus.Size = new System.Drawing.Size(148, 20);
            this.labStatus.TabIndex = 0;
            this.labStatus.Text = "Überwachung steht";
            // 
            // cmdStart
            // 
            this.cmdStart.AutoSize = true;
            this.cmdStart.Enabled = false;
            this.cmdStart.Location = new System.Drawing.Point(40, 109);
            this.cmdStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdStart.Name = "cmdStart";
            this.cmdStart.Size = new System.Drawing.Size(186, 51);
            this.cmdStart.TabIndex = 1;
            this.cmdStart.Text = "Überwachung starten";
            this.cmdStart.UseVisualStyleBackColor = true;
            this.cmdStart.Click += new System.EventHandler(this.CmdStart_Click);
            // 
            // cmdStop
            // 
            this.cmdStop.AutoSize = true;
            this.cmdStop.Enabled = false;
            this.cmdStop.Location = new System.Drawing.Point(236, 109);
            this.cmdStop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdStop.Name = "cmdStop";
            this.cmdStop.Size = new System.Drawing.Size(186, 51);
            this.cmdStop.TabIndex = 2;
            this.cmdStop.Text = "Überwachung stoppen";
            this.cmdStop.UseVisualStyleBackColor = true;
            this.cmdStop.Click += new System.EventHandler(this.CmdStop_Click);
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(22, 238);
            this.txtLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(673, 561);
            this.txtLog.TabIndex = 11;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(730, 238);
            this.txtFax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFax.Multiline = true;
            this.txtFax.Name = "txtFax";
            this.txtFax.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFax.Size = new System.Drawing.Size(673, 561);
            this.txtFax.TabIndex = 16;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(1064, 54);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(312, 169);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // menuHauptmenü
            // 
            this.menuHauptmenü.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuHauptmenü.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.einstellungenToolStripMenuItem});
            this.menuHauptmenü.Location = new System.Drawing.Point(0, 0);
            this.menuHauptmenü.Name = "menuHauptmenü";
            this.menuHauptmenü.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuHauptmenü.Size = new System.Drawing.Size(1431, 34);
            this.menuHauptmenü.TabIndex = 20;
            this.menuHauptmenü.Text = "menuHauptmenü";
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.AutoSize = false;
            this.einstellungenToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            this.einstellungenToolStripMenuItem.Size = new System.Drawing.Size(117, 28);
            this.einstellungenToolStripMenuItem.Text = "Einstellungen";
            this.einstellungenToolStripMenuItem.Click += new System.EventHandler(this.EinstellungenToolStripMenuItem_Click);
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1431, 835);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtFax);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.cmdStop);
            this.Controls.Add(this.cmdStart);
            this.Controls.Add(this.labStatus);
            this.Controls.Add(this.menuHauptmenü);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuHauptmenü;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "formMain";
            this.Text = "Alarmfax-Verzeichnis-Überwacher";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuHauptmenü.ResumeLayout(false);
            this.menuHauptmenü.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labStatus;
        private System.Windows.Forms.Button cmdStart;
        private System.Windows.Forms.Button cmdStop;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        public System.Windows.Forms.MenuStrip menuHauptmenü;
        public SettingsRA mySettings;
    }
}

