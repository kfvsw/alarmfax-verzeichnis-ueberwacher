﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class formSettings : Form
    {
        private FormMain myMainForm;

        public formSettings(FormMain MyMainForm)
        {
            InitializeComponent();
            myMainForm = MyMainForm;
            UpdatePathField(txtSourceFolder);
            UpdatePathField(txtTargetFolder);
            UpdatePathField(txtArchiveFolder);
            UpdatePathField(txtTargetFolder2);
            UpdatePathField(txtTesseractFolder);
            this.txtTargetFolder.Text = MyMainForm.mySettings.pathTarget1;
            this.txtSourceFolder.Text = MyMainForm.mySettings.pathSource;
            this.txtArchiveFolder.Text = MyMainForm.mySettings.pathArchive;
            this.txtTargetFolder2.Text = MyMainForm.mySettings.pathTarget2;
            this.txtFailedFax.Text = MyMainForm.mySettings.pathFailed;
            this.txtFilter.Text = MyMainForm.mySettings.filter;
            this.txtTesseractFolder.Text = MyMainForm.mySettings.pathTesseract;
            this.chkPrint.Checked = MyMainForm.mySettings.automaticPrint;
            this.chkWeb.Checked = MyMainForm.mySettings.automaticSend2Web;
            this.txtDelay1.Text = myMainForm.mySettings.delay1.ToString();
            this.txtDelay2.Text = myMainForm.mySettings.delay2.ToString();
            this.txtDelay3.Text = myMainForm.mySettings.delay3.ToString();
            this.txtRecipient.Text = myMainForm.mySettings.recipient;
            this.txtRecipientProvide.Text = myMainForm.mySettings.recipientProvide;
            this.txtPhpScript.Text = myMainForm.mySettings.PhpScript;
        }

        private void CmdChangeSource_Click(object sender, EventArgs e)
        {
            DialogResult res;
            res = this.folderBrowserDialog.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                myMainForm.mySettings.pathSource = folderBrowserDialog.SelectedPath + @"\";
                UpdatePathField(txtSourceFolder);
            }
        }

        private void CmdChangeTarget_Click(object sender, EventArgs e)
        {
            DialogResult res;
            res = this.folderBrowserDialog.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                myMainForm.mySettings.pathTarget1 = folderBrowserDialog.SelectedPath + @"\";
                UpdatePathField(txtTargetFolder);
            }
        }

        private void CmdChangeArchive_Click(object sender, System.EventArgs e)
        {
            DialogResult res;
            res = this.folderBrowserDialog.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                myMainForm.mySettings.pathArchive = folderBrowserDialog.SelectedPath + @"\";
                UpdatePathField(txtArchiveFolder);
            }
        }

        private void CmdChangeTarget2_Click(object sender, EventArgs e)
        {
            DialogResult res;
            res = this.folderBrowserDialog.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                myMainForm.mySettings.pathTarget2 = folderBrowserDialog.SelectedPath + @"\";
                UpdatePathField(txtTargetFolder2);
            }
        }

        private void CmdChangeTesseract_Click(object sender, EventArgs e)
        {
            DialogResult res;
            res = this.folderBrowserDialog.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                myMainForm.mySettings.pathTesseract = folderBrowserDialog.SelectedPath + @"\";
                UpdatePathField(txtTesseractFolder);
            }
        }

        private void UpdatePathField(TextBox myTextBox)
        {
            if (myTextBox == this.txtArchiveFolder) txtArchiveFolder.Text = myMainForm.mySettings.pathArchive;
            if (myTextBox == this.txtTargetFolder2) txtTargetFolder2.Text = myMainForm.mySettings.pathTarget2;
            if (myTextBox == this.txtTesseractFolder)  txtTesseractFolder.Text = myMainForm.mySettings.pathTesseract;
            if (myTextBox == this.txtTargetFolder) txtTargetFolder.Text = myMainForm.mySettings.pathTarget1;
            if (myTextBox == this.txtSourceFolder) txtSourceFolder.Text = myMainForm.mySettings.pathSource;
            if (myTextBox == this.txtFailedFax) txtFailedFax.Text = myMainForm.mySettings.pathFailed;
        }

        private void FormSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            myMainForm.mySettings.pathArchive = this.txtArchiveFolder.Text;
            myMainForm.mySettings.pathTarget1 = this.txtTargetFolder.Text;
            myMainForm.mySettings.pathTarget2 = this.txtTargetFolder2.Text;
            myMainForm.mySettings.pathTesseract = this.txtTesseractFolder.Text;
            myMainForm.mySettings.pathSource = this.txtSourceFolder.Text;
            myMainForm.mySettings.pathFailed = this.txtFailedFax.Text;
            myMainForm.mySettings.filter = this.txtFilter.Text;
            myMainForm.mySettings.automaticPrint = chkPrint.Checked ? true : false;
            myMainForm.mySettings.automaticSend2Web = chkWeb.Checked ? true : false;
            myMainForm.mySettings.delay1 = txtDelay1.Text != "" ? Int32.Parse(txtDelay1.Text) : 0;
            myMainForm.mySettings.delay2 = txtDelay2.Text != "" ? Int32.Parse(txtDelay2.Text) : 0;
            myMainForm.mySettings.delay3 = txtDelay3.Text != "" ? Int32.Parse(txtDelay3.Text) : 0;
            myMainForm.mySettings.recipient = txtRecipient.Text;
            myMainForm.mySettings.recipientProvide = txtRecipientProvide.Text;
            myMainForm.mySettings.PhpScript = txtPhpScript.Text;
            myMainForm.mySettings.WriteApplicationSettings();
            myMainForm.CheckPaths();
        }

        private void TextDelay1_KeyDown(object sender, KeyEventArgs e)
        {
            Debug.WriteLine(e.KeyValue);
            if ((e.KeyValue >= 96 && e.KeyValue <= 105) || (e.KeyValue >= 48 && e.KeyValue <= 57) || (e.KeyValue == 8) || (e.KeyValue == 46)) e.SuppressKeyPress = false;
            else e.SuppressKeyPress = true;
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {
            toolTipDelay.ShowAlways = true;
            toolTipDelay.AutoPopDelay = 10000;
            toolTipDelay.SetToolTip(this.labDelay1, "Hier kann eine Wartezeit angegeben werden, die das Programm wartet,\r\nnachdem ein Fax-PDF im Überwachungsverzeichnis detektiert wurde.\r\nDer Wert muss in Millisekunden eingegeben werden.");
            toolTipDelay.SetToolTip(this.labDelay2, "Hier kann eine Zeitspanne angegeben werden, in der sich die Dateigröße des Fax-PDFs nicht mehr ändern darf,\r\nbevor mit der Texterkennung begonnen wird.\r\nDer Wert muss in Millisekunden eingegeben werden.");
            toolTipDelay.SetToolTip(this.labDelay3, "Hier wird die Zykluszeit festgelegt, in der das Programm nach neuen Dateien sucht.\r\nAchtung! Zu niedrige Werte lasten den Rechner hoch aus (Empfehlung: Wert > 200).\r\nDer Wert muss in Millisekunden eingegeben werden.");
            EnableControls();
        }

        private void TxtPhpScript_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        private void EnableControls()
        {
            if (this.txtPhpScript.Text == "")
            {
                this.chkWeb.Enabled = false;
                this.chkWeb.Checked = false;
            }
            else
            {
                this.chkWeb.Enabled = true;
            }
        }

        private void CmdChangeFailed_Click(object sender, EventArgs e)
        {
            DialogResult res;
            res = this.folderBrowserDialog.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                myMainForm.mySettings.pathFailed = folderBrowserDialog.SelectedPath + @"\";
                UpdatePathField(txtFailedFax);
            }
        }


    }
}
