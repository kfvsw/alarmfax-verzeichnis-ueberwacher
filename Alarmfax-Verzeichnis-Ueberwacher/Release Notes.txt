*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.6.3.2

- Umstellung auf Tesseract 5

*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.6.3.1

Hotfix ILS SWE: 
- Bugfix Ausgabe Stichwort
- Bugfix nicht gefundene Einsatzmittel / Feuerwehren ("="-Bug)

*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.6.3

�nderungen zur vorherigen Version
- ILS W�: Umfassendes Refactoring: Kein Absturz bei Fax ohne Koordinaten (trifft bei HvO-Eins�tzen zu), kein Absturz bei nicht gefundenen Textmarken


*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.6.2.1

Hotfix
- Kein Absturz bei mehrseitigen TIFs; Hinweis: mehrseitige TIFs werden aktuell nur auf einer Seite ausgewertet


*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.6.2

�nderungen zur vorherigen Version
- Unterst�tzung f�r zweites Zielverzeichnis


*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.6.1

�nderungen zur vorherigen Version
- Parser erweitert: ILS TS 


*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.6

�nderungen zur vorherigen Version
- Parser erweitert: ILS AM 


*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.5.1

�nderungen zur vorherigen Version
- Anpassung Standardwert f�r Tesseract-Suchverzeichnis


*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.5

�nderungen zur vorherigen Version
- Erkennung des Absenders nicht mehr �ber Fax-Sende-Nummer, sondern �ber den Eintrag "Absender"
- Keine Auswertung der Sende-Nummer in der Kopfzeile des Fax -> keine Abh�ngigkeit von der Skalierung des Fax (z.B. breiterer Rand)
- Refactoring: Vorbereitung f�r weitere Fax-Parser
- Vorbereitung Schriftgr��enanpassung


*****************************************************************
Alarmfax-Verzeichnis-�berwacher 2.4

�nderungen zur vorherigen Version
- Unterst�tzung neues Fax-Format ILS W� (ge�nderter Header, Koordinaten)
- Bugfix Fax-Format ILS W�: Umbruch bei Einsatzmittel wird jetzt korrekt erkannt
- Bei Programmstart wird �berwachung automatisch gestartet, sofern Systemcheck ok war
- Alle E-Mail-Adressaten auf BCC
- Bugfix: Kein Absturz mehr, wenn �berwachtes Verzeichnis nicht mehr erreichbar ist. Erneute Pr�fung alle 10 s.
