﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Windows;
using System.Reflection;
using System.Net.Mail;
using System.Net.Mime;

namespace WindowsFormsApplication1
{   
    public partial class FormMain : Form
    {
        private Queue filelist = new Queue();
        private Queue ignorlist = new Queue();
        private Stopwatch sw = new Stopwatch();
        private int exceptionCounter = 0;
        private bool isSourceReachable = true;
        private bool useArchive2 = false;
        public FormMain()
        {
            InitializeComponent();
            this.Text = "Alarmfax-Verzeichnis-Überwacher v" + Assembly.GetExecutingAssembly().GetName().Version;
            this.mySettings = new SettingsRA();
            this.CheckPaths();
            if(this.cmdStart.Enabled)
            {
                CmdStart_Click();
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            string fileInWork = "";
            List<string> recipients;
            bool exceptionDetected = false;
            // avoid nested timer
            timer1.Enabled = false;
            try
            {
                string[] filePaths = new string[0];
                if (Directory.Exists(mySettings.pathSource))
                {
                    if(!isSourceReachable)
                    {
                        timer1.Interval = 200;
                        AddLogEntry("Überwachtes Verzeichnis wieder erreichbar");
                        isSourceReachable = true;
                    }

                    filePaths = GetFiles(mySettings.pathSource, mySettings.filter, SearchOption.TopDirectoryOnly);

                    int i;

                    for (i = 0; i < filePaths.Count(); i++)
                    {
                        if (!ignorlist.Contains(filePaths[i]))
                        {
                            AddLogEntry("Fax " + Path.GetFileName(filePaths[i]) + " empfangen");
                            fileInWork = filePaths[i];
                            sw.Restart();
                            Alarmfax af = new Alarmfax(filePaths[i], mySettings.pathArchive, mySettings.automaticSend2Web, mySettings.PhpScript);
                            System.Threading.Thread.Sleep(mySettings.delay1);

                            // Step 1: check access
                            if (!af.CheckAccess())
                            {
                                ignorlist.Enqueue(fileInWork);
                                AddLogEntry("Kein Zugriff auf Fax " + fileInWork + ", wird ab sofort ignoriert!");
                                continue; // jumps to the beginning of the for loop -> no further steps with this file
                            }
                            AddLogEntry("PDF beim " + (af.GetAccessTrys() + 1).ToString() + ". Leseversuch bereit");

                            // Step 2: check if file size is constant
                            if (!af.CheckFileSize(mySettings.delay2))
                            {
                                ignorlist.Enqueue(fileInWork);
                                AddLogEntry("Faxdatei " + fileInWork + " hat keine konstante Dateigröße, wird ab sofort ignoriert!");
                                continue; // jumps to the beginning of the for loop -> no further steps with this file
                            }
                            AddLogEntry("PDF-Größe bei der " + (af.GetSizeChecks() + 1).ToString() + ". Überprüfung konstant (" + af.GetFileSize() + " Bytes)");

                            // Step 3: extract tif
                            fileInWork = af.ExtractTif(); // TODO Rückgabe von extractTif auswerten; bei Fehlern Fax-Datei auf Ignore-List setzen, keine weitere Bearbeitung

                            // Step 4: cut tif
                            af.CutTif();

                            // Step 5: cut sender number
                            //af.cutSenderNumber();

                            // Step 6: do ocr 
                            af.DoOcr(mySettings.pathTesseract);

                            // Step 7: do ocr on sender number
                            //af.doOcrSenderNumber(mySettings.pathTesseract);

                            // Step 8: generate Output
                            if (!af.GenerateOutput(mySettings.pathTarget1, this.txtFax))
                            {
                                AddLogEntry(af.ErrorText);
                                SendErrorMail(af.ErrorText, fileInWork);
                            }
                            else
                            {
                                AddLogEntry("Fax " + Path.GetFileName(filePaths[i]) + " bearbeitet");
                                if (this.useArchive2)
                                {
                                    af.GenerateOutput(mySettings.pathTarget2, this.txtFax);
                                }
                            }

                            sw.Stop();

                            if (mySettings.automaticPrint == true) // TODO put this setting into configuration
                            {
                                af.PrintPdf();
                            }

                            recipients = new List<string>();
                            if (mySettings.recipientProvide != "")
                            {
                                recipients.Add(mySettings.recipientProvide);
                            }
                            SendInfoMail("neues Alarmfax", "siehe Anhang", recipients.ToArray(), fileInWork);
                            fileInWork = "";
                        }
                    }
                }
                else // directory not available
                {
                    if (isSourceReachable)
                    {
                        timer1.Interval = 10000;
                        AddLogEntry("Überwachtes Verzeichnis nicht erreichbar!");
                        isSourceReachable = false;
                    }
                }
                
                if (exceptionCounter > 0)
                { 
                    exceptionCounter--;
                }
            }
            catch (Exception ex)
            {
                exceptionCounter++;
                if (exceptionCounter > 10)
                {
                    // emergency stop: to many exceptions detected
                    recipients = new List<string>();
                    if(mySettings.recipient != "") 
                    {
                        recipients.Add(mySettings.recipient);
                    }
                    SendInfoMail("Fehler Alarmfax-Verzeichnis-Überwacher v" + Assembly.GetExecutingAssembly().GetName().Version, "Fehler beim Ausführen des Alarmfax-Verzeichnis-Überwachers: " + ex.ToString(), recipients.ToArray());
                    System.Windows.Forms.MessageBox.Show(ex.ToString());
                    System.Windows.Forms.Application.Exit(); 
                }
                recipients = new List<string>();
                recipients.Add(""); // TODO add recipient e-mail address as configuration parameter
                SendInfoMail("Fehler Alarmfax-Verzeichnis-Überwacher v" + Assembly.GetExecutingAssembly().GetName().Version, "Fehler beim Ausführen des Alarmfax-Verzeichnis-Überwachers: " + ex.ToString(), recipients.ToArray(), fileInWork);
                AddLogEntry("Unerwarteter Fehler detektiert, Absturzmeldung gesendet.");
                exceptionDetected = true;

            }
            if (exceptionDetected)
            {
                try
                {
                    if(fileInWork != "")
                    {
                        File.Move(fileInWork, mySettings.pathFailed);
                        AddLogEntry("Fehler bei der Auswertung des Fax " + fileInWork + ": ohne Bearbeitung nach " + mySettings.pathFailed + " verschoben.");
                    }
                }
                catch
                {
                    ignorlist.Enqueue(fileInWork);
                    AddLogEntry("Kein Zugriff auf Fax " + fileInWork + ", wird ab sofort ignoriert!");
                }

            }
            timer1.Enabled = true;
        }

        private void SendErrorMail(string info, string fileInWork)
        {
            // currently not used
            //List<string> recipients;
            //recipients = new List<string>();
            // TODO put here e-mail recipients
            //SendInfoMail("Fehler Alarmfax-Verzeichnis-Überwacher v" + Assembly.GetExecutingAssembly().GetName().Version, "Fehler beim Ausführen des Alarmfax-Verzeichnis-Überwachers: " + info, recipients.ToArray(), fileInWork);
        }

        private void CmdStart_Click(object sender = null, EventArgs e = null)
        {
            this.labStatus.Text = "Überwachung läuft";
            this.timer1.Interval = mySettings.delay3;
            this.timer1.Enabled = true;
            EnableCommandButtons(false);
            AddLogEntry("Überwachung gestartet");
            mySettings.WriteApplicationSettings();
        }

        private void CmdStop_Click(object sender, EventArgs e)
        {
            this.labStatus.Text = "Überwachung steht";
            this.timer1.Enabled = false;
            this.ignorlist.Clear();
            this.filelist.Clear();
            EnableCommandButtons(true);
            AddLogEntry("Überwachung gestoppt");
        }

        private void EnableCommandButtons(bool enable)
        {
            this.cmdStart.Enabled = enable;
            this.cmdStop.Enabled = !enable;
            this.menuHauptmenü.Enabled = enable;
        }

        public void CheckPaths()
        {
            bool error = false;
            if (mySettings.pathSource == "")
            {
                AddLogEntry("Fehler! Kein Angabe für 'Überwachtes Verzeichnis'");
                error = true;
            }
            else
            {
                if (!Directory.Exists(mySettings.pathSource))
                {
                    AddLogEntry("Fehler! " + mySettings.pathSource + " konnte nicht gefunden werden"); 
                    error = true;
                }
            }
            if (mySettings.pathTarget1 == "")
            {
                AddLogEntry("Fehler! Kein Angabe für 'Zielverzeichnis'");
                error = true;
            }
            else
            {
                if (!Directory.Exists(mySettings.pathTarget1))
                {
                    AddLogEntry("Fehler! " + mySettings.pathTarget1 + " konnte nicht gefunden werden");
                    error = true;
                }
            }
            if (mySettings.pathArchive == "")
            {
                AddLogEntry("Fehler! Kein Angabe für 'Archivverzeichnis1'");
                error = true;
            }
            else
            {
                if (!Directory.Exists(mySettings.pathArchive))
                {
                    AddLogEntry("Fehler! " + mySettings.pathArchive + " konnte nicht gefunden werden");
                    error = true;
                }
            }
            if (mySettings.pathTarget2 == "")
            {
                AddLogEntry("Hinweis! Kein Angabe für 'Archivverzeichnis2'");
                this.useArchive2 = false;
            }
            else
            {
                if (!Directory.Exists(mySettings.pathTarget2))
                {
                    AddLogEntry("Hinweis! " + mySettings.pathTarget2 + " konnte nicht gefunden werden");
                    this.useArchive2 = false;
                }
                else
                {
                    this.useArchive2 = true;
                }
            }

            if (mySettings.pathFailed == "")
            {
                AddLogEntry("Fehler! Kein Angabe für 'Verzeichnis fehlgeschlagene Faxe'");
                error = true;
            }
            else
            {
                if (!Directory.Exists(mySettings.pathFailed))
                {
                    AddLogEntry("Fehler! " + mySettings.pathFailed + " konnte nicht gefunden werden");
                    error = true;
                }
            }
            if (mySettings.pathTesseract == "")
            {
                AddLogEntry("Fehler! Kein Angabe für 'Verzeichnis Tesseract'");
                error = true;
            }
            else
            {
                if (!Directory.Exists(mySettings.pathTesseract))
                {
                    AddLogEntry("Fehler! " + mySettings.pathTesseract + " konnte nicht gefunden werden");
                    error = true;
                }
            }
            cmdStart.Enabled = !error;
            if (!error) AddLogEntry("Systemcheck ok");
        }
        private string[] GetFiles(string SourceFolder, string Filter, System.IO.SearchOption searchOption)
        {
            // ArrayList will hold all file names
            ArrayList alFiles = new ArrayList();

            // Create an array of filter string
            string[] MultipleFilters = Filter.Split('|');

            // for each filter find mathing file names
            foreach (string FileFilter in MultipleFilters)
            {
                // add found file names to array list
                alFiles.AddRange(Directory.GetFiles(SourceFolder, FileFilter, searchOption));
            }

            // returns string array of relevant file names
            return (string[])alFiles.ToArray(typeof(string));
        }
        private void AddLogEntry(string strEntry)
        {
            txtLog.Text = DateTime.Now.ToString() + ": " + strEntry + "\r\n" + txtLog.Text;
            txtLog.Update();
        }

        private void SendInfoMail(string subject, string content, string[] recipients, string file = "")
        {
            try
            {
                Attachment data = null;
                MailMessage Email = new MailMessage();
                MailAddress Sender = new MailAddress(""); // TODO add sender e-mail address as configuration parameter
                Email.From = Sender;
                Email.ReplyToList.Add(""); // TODO add replyTo e-mail address as configuration parameter
                foreach (string recipient in recipients)
                {
                    Email.Bcc.Add(recipient);
                }
                Email.Subject = subject;
                if (file != "")
                {
                    data = new Attachment(file, MediaTypeNames.Application.Pdf);
                    Email.Attachments.Add(data);
                }
                Email.Body = content;
                string ServerName = ""; // TODO add servername as configuration parameter
                int Port = 587; // TODO add smtp port as configuration parameter
                SmtpClient MailClient = new SmtpClient(ServerName, Port)
                {
                    EnableSsl = true, // TODO add EnableSsl as configuration parameter
                    DeliveryMethod = SmtpDeliveryMethod.Network
                };
                string UserName = ""; // TODO add username as configuration parameter
                string Password = ""; // TODO add password as configuration parameter
                System.Net.NetworkCredential Credentials = new System.Net.NetworkCredential(UserName, Password);
                MailClient.UseDefaultCredentials = true;
                MailClient.Credentials = Credentials;
                if (!this.mySettings.NoEmail) // only send mails if allowed to
                {
                    MailClient.Send(Email);
                }
                data.Dispose();
            }
            catch
            {
                // no error handling, just to keep software running
            }
        }

        private void EinstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formSettings MyFormSettings = new formSettings(this);
            MyFormSettings.ShowDialog(this);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            // TODO adaption for high resolution screens
            //System.Drawing.Font BigFont;

            //BigFont = new System.Drawing.Font("Microsoft Sans Serif", 12);
            //this.einstellungenToolStripMenuItem.Font = BigFont;
            //this.Font = BigFont;
        }
    }
}
