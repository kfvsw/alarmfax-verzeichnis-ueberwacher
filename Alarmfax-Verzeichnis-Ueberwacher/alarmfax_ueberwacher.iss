#define AppVer GetFileVersion("bin\Release\Alarmfax-Verzeichnis-Überwacher.exe")

[Languages]
Name: "de"; MessagesFile: "compiler:Languages\German.isl"

[Setup]
AppName=Alarmfax-Verzeichnis-Überwacher
AppVersion={#AppVer}
DefaultDirName={pf}\AlFaVeUeWa
DefaultGroupName=Alarmfax-Verzeichnis-Überwacher
UninstallDisplayIcon={app}\MyProg.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Alarmfax-Verzeichnis-Ueberwacher
OutputBaseFilename=Setup_Alarmfax-Verzeichnis-Ueberwacher_{#AppVer}
AppPublisher=Ralf weippert
DisableProgramGroupPage=No
InfoBeforeFile=Release Notes.txt
AppCopyright=Copyright (C) 2019 Ralf Weippert
AppContact=ralf.weippert@gmail.com
DisableWelcomePage=no
LicenseFile=Haftungsausschluss.txt

[Files]
Source: "bin\Release\Alarmfax-Verzeichnis-Überwacher.exe"; DestDir: "{app}"
Source: "Release Notes.txt"; DestDir: "{app}"
Source: "Haftungsausschluss.txt"; DestDir: "{app}"
Source: "bin\Release\Spire.License.dll"; DestDir: "{app}"
Source: "bin\Release\Spire.License.xml"; DestDir: "{app}"
Source: "bin\Release\Spire.Pdf.dll"; DestDir: "{app}"
Source: "bin\Release\Spire.Pdf.xml"; DestDir: "{app}"
Source: "C:\Program Files\Tesseract-OCR\*"; DestDir: "{app}\Tesseract-OCR"; Flags: recursesubdirs

[Icons]
Name: "{group}\Alarmfax-Verzeichnis-Überwacher"; Filename: "{app}\Alarmfax-Verzeichnis-Überwacher.exe"