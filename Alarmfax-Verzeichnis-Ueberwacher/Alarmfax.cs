﻿/*
    Copyright (C) 2020 Ralf Weippert - All Rights Reserved    

    This file is part of Alarmfax-Verzeichnis-Überwacher.

    Alarmfax-Verzeichnis-Überwacher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Alarmfax-Verzeichnis-Überwacher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alarmfax-Verzeichnis-Überwacher.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Alarmfax-Verzeichnis-Überwacher.

    Das Programm Alarmfax-Verzeichnis-Überwacher ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Das Programm Alarmfax-Verzeichnis-Überwacher wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Spire.Pdf;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    enum Filetypes {PDF, TIF, unknown};
    class Alarmfax
    {
        private string filename;
        private Filetypes filetype;
        private string timestamp;
        public string sender_number;
        private string pathFilenameOriginal;
        private string pathFilenameArchive;
        private string pathArchive;
        private Int16 accessTrys = 0;
        private Int16 sizeChecks = 0;
        private Int64 FileSize = 0;
        private Int16 numberOfImages = 0;
        private bool CanPushToWeb = false;
        private string PhpScript = "";
        public string ErrorText = "";
        private FaxReferencePoints FaxRefPoints;

        public Alarmfax(string pathO, string pathA, bool canPushToWeb, string phpScript)
        {
            filename = Path.GetFileNameWithoutExtension(pathO);
            timestamp = DateTime.Now.ToString("yyyymmdd_HHMMss") + "_";
            pathFilenameOriginal = pathO;
            pathArchive = pathA;
            CanPushToWeb = canPushToWeb;
            PhpScript = phpScript;
            switch(Path.GetExtension(pathO).ToUpper())
            {
                case ".PDF":
                    filetype = Filetypes.PDF;
                    break;
                case ".TIF":
                    filetype = Filetypes.TIF;
                    break;
                case ".TIFF":
                    filetype = Filetypes.TIF;
                    break;
                default:
                    filetype = Filetypes.unknown;
                    break;
            }
        }
        public bool CheckAccess() 
        {
            Stream s;
            while (accessTrys < 1000)
            {
                try
                {
                    s = File.Open(pathFilenameOriginal, FileMode.Open, FileAccess.Read, FileShare.None);
                    s.Dispose();
                    return true;
                }
                catch 
                {
                    accessTrys++;
                }
            }
            return false;
        }
        public bool CheckFileSize(Int32 delay)
        {
            long fileSize;
            FileInfo fi;
            while (sizeChecks < 1000)
            {
                fi = new FileInfo(pathFilenameOriginal);
                Debug.WriteLine(fi.Length);
                fileSize = fi.Length;
                System.Threading.Thread.Sleep(delay);
                if(fileSize == 0)
                {
                    Debug.WriteLine("continue");
                    continue;
                }
                fi = new FileInfo(pathFilenameOriginal);
                Debug.WriteLine(fi.Length);
                if (fileSize == fi.Length)
                {
                    FileSize = fi.Length;
                    return true;
                }
                sizeChecks++;
            }
            return false;

        }
        public Int16 GetSizeChecks()
        {
            return sizeChecks;
        }
        public string GetFileSize()
        {
            return FileSize.ToString();
        }
        public Int16 GetAccessTrys()
        {
            return accessTrys;
        }
        public string ExtractTif()
        {

            if(this.filetype == Filetypes.PDF)
            {
                PdfDocument document = new PdfDocument();
                document.LoadFromFile(pathFilenameOriginal);

                // IList<Image> images = new List<Image>(); TODO it could be that there are more than one pics on page

                foreach (PdfPageBase page in document.Pages)
                {
                    foreach (Image image in page.ExtractImages())
                    {
                        numberOfImages++;
                        image.Save(pathArchive + timestamp + filename + "_" + numberOfImages.ToString() + ".tif", ImageFormat.Tiff);
                    }
                }
                document.Close();
            }
            if(this.filetype == Filetypes.TIF)
            {
                // just rename tif according to convention
                numberOfImages++;
                File.Copy(this.pathFilenameOriginal, pathArchive + timestamp + filename + "_" + numberOfImages.ToString() + ".tif");
            }
            // move original to archive
            Int16 counter = 1;
            pathFilenameArchive = pathArchive + timestamp + filename + Path.GetExtension(pathFilenameOriginal);
            while (File.Exists(pathFilenameArchive))
            {
                pathFilenameArchive = pathArchive + timestamp + filename + "_" + counter.ToString() + Path.GetExtension(pathFilenameOriginal);
            }
            File.Move(pathFilenameOriginal, pathFilenameArchive);
            return pathFilenameArchive;
        }
        public void CutTif()
        {
            Int16 pic;
            for (pic = 1; pic <= numberOfImages; pic++)
            {
                System.Drawing.Image bild = System.Drawing.Image.FromFile(pathArchive + timestamp + filename + "_" + pic.ToString() + ".tif");
                Bitmap bmOriginal = new Bitmap(bild);
                if(this.FaxRefPoints == null)
                {
                    // define reference points only for the first page
                    this.FaxRefPoints = new FaxReferencePoints(bmOriginal);
                }
                Bitmap bmCutted = new Bitmap(bild.Width, bild.Height - this.FaxRefPoints.GetLowestReferenceRow());
                Graphics g = Graphics.FromImage(bmCutted);
                g.DrawImage(bmOriginal, 0, -this.FaxRefPoints.GetLowestReferenceRow() + 1);
                bmCutted.Save(pathArchive + timestamp + filename + "_" + pic.ToString() + "_cutted.tif");
                g.Dispose();
                bild.Dispose();
                bmOriginal.Dispose();
                bmCutted.Dispose();
            }
        }

        public void CutSenderNumber()
        {
            System.Drawing.Image bild = System.Drawing.Image.FromFile(pathArchive + timestamp + filename + "_1.tif");
            Bitmap bmCutted = new Bitmap(318, 22);
            Graphics g = Graphics.FromImage(bmCutted);
            RectangleF sourceRect = new RectangleF(317+this.FaxRefPoints.GetLeftBorder()-59,this.FaxRefPoints.GetLowestReferenceRow()-31,318,22);
            RectangleF destinationRect = new RectangleF(0,0,318,22);
            g.DrawImage(bild, destinationRect, sourceRect, GraphicsUnit.Pixel);
            bmCutted.Save(pathArchive + timestamp + filename + "_sender_cutted.tif");
            g.Dispose();
            bild.Dispose();
            bmCutted.Dispose();
        }
        public void DoOcr(string pathTesseract)
        {
            Int16 pic;
            string fileNameTif;
            string fileNameTxt;
            string textBuffer = "";
            for (pic = 1; pic <= numberOfImages; pic++)
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
                {
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                    FileName = pathTesseract + "tesseract.exe",
                    WorkingDirectory = pathArchive
                };
                fileNameTif = timestamp + filename + "_" + pic.ToString() + "_cutted.tif";
                fileNameTxt = timestamp + filename + "_" + pic.ToString();
                startInfo.Arguments = fileNameTif + " " + fileNameTxt + " -l deu";
                startInfo.RedirectStandardOutput = false;
                startInfo.UseShellExecute = true;
                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit();
                process.Dispose();
                textBuffer += System.IO.File.ReadAllText(pathArchive + timestamp + filename + "_" + pic.ToString() + ".txt");
            }
            System.IO.File.WriteAllText(pathArchive + timestamp + filename + ".txt", textBuffer);
        }
        public void DoOcrSenderNumber(string pathTesseract)
        {
            string fileNameTif;
            string fileNameTxt;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
            {
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                FileName = pathTesseract + "tesseract.exe",
                WorkingDirectory = pathArchive
            };
            fileNameTif = timestamp + filename + "_sender_cutted.tif";
            fileNameTxt = timestamp + filename + "_sender";
            startInfo.Arguments = fileNameTif + " " + fileNameTxt + " nobatch digits_kfv";
            startInfo.RedirectStandardOutput = false;
            startInfo.UseShellExecute = true;
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
            process.Dispose();
            sender_number = System.IO.File.ReadAllText(pathArchive + timestamp + filename + "_sender.txt");
        }
        private void PutOut(System.IO.StreamWriter sw, System.Windows.Forms.TextBox tb, string outputstring) 
        {
            sw.WriteLine(outputstring);
            if (tb.Text == "")
            {
                tb.Text = outputstring;
            }
            else
            {
                tb.Text = tb.Text + "\r\n" + outputstring;
            }
        }
        public bool GenerateOutput(string pathOut, System.Windows.Forms.TextBox tb)
        {
            // use unique start and end tags
            // remove line feeds which are not necessary
            string line;
            string filename_source = pathArchive + timestamp + filename + ".txt";       // filename of sourcefile for output generation
            string filename_tempor = pathArchive + timestamp + filename + "_temp.txt";  // filename of temporary file
            string filename_destin = pathOut + timestamp + filename + ".txt";           // filename ready for alarm
            Alarmfaxinhalt aiPreliminary = new Alarmfaxinhalt();
            //ai.setAbsenderFaxNr(sender_number);
            System.IO.StreamReader file = new System.IO.StreamReader(filename_source);
            while ((line = file.ReadLine()) != null)
            {
                if (line != "")
                {
                    line = line.Replace("\t", " ");
                    line = line.Replace("  ", " ");
                    line = line.Replace("Uchtelhausen", "Üchtelhausen");
                    line = line.Replace("ELiter3", "{Liter}");
                    line = line.Replace("ELiterl", "{Liter}");
                    line = line.Replace("ELiterL", "{Liter}");
                    line = line.Replace("{Liter}", "");
                    line = line.Replace("Pressluftatmer", "PA");
                    line = line.Replace("(Gerät + Maske)", "");
                    line = line.Replace("Tragkraftspritze TS 8/8, PFPN 10-1000", "TS");
                    line = line.Replace("[TS, PFPN]", "TS");
                    line = line.Replace("Rettungssatz, hydraulisch", "hyd-RS");
                    line = line.Replace("Gruppe (Takt. Einheit, Dispo)", "Gruppe");
                    line = line.Replace("Unterfr", "");
                    line = line.Replace("TEXTBAusTEiNE", "TEXTBAUSTEIN");
                    line = line.Replace("TEXTBAusTEiN", "TEXTBAUSTEIN");
                    aiPreliminary.Content.Add(line);
                }
            }
            file.Close();
            file.Dispose();

            string[] catchwords = { "Absender", "Einsatz", "EINSATZORT", "Ort", "EINSATZMITTEL"};

            if (aiPreliminary.CheckMinimumContentWords(catchwords) != "")
            {
                ErrorText = "Fehler: Folgende Schlagworte konnten im Fax nicht gefunden werden: " + ErrorText;
                return false;
            }

            // ABSENDER
            aiPreliminary.SetAbsender(aiPreliminary.ExtractSingleLineContent("Absender"));


            if(aiPreliminary.IsIlsSw())
            {
                AlarmfaxinhaltIlsSw ai = new AlarmfaxinhaltIlsSw(aiPreliminary.Content, aiPreliminary.GetAbsender(false));
                ai.ParseContent();
                aiPreliminary = ai;

            }
            if(aiPreliminary.IsIlsWue())
            {
                AlarmfaxinhaltIlsWue ai = new AlarmfaxinhaltIlsWue(aiPreliminary.Content, aiPreliminary.GetAbsender(false));
                ai.ParseContent();
                aiPreliminary = ai;
            }

            if(aiPreliminary.IsIlsAm())
            {
                AlarmfaxinhaltIlsAm ai = new AlarmfaxinhaltIlsAm(aiPreliminary.Content, aiPreliminary.GetAbsender(false));
                ai.ParseContent();
                aiPreliminary = ai;
            }

            if(aiPreliminary.IsIlsTs())
            {
                AlarmfaxinhaltIlsTs ai = new AlarmfaxinhaltIlsTs(aiPreliminary.Content, aiPreliminary.GetAbsender(false));
                ai.ParseContent();
                aiPreliminary = ai;
            }


            tb.Text = "";
            System.IO.StreamWriter file_writer = new System.IO.StreamWriter(filename_tempor);
            PutOut(file_writer, tb, aiPreliminary.GetAbsenderFaxNr());
            PutOut(file_writer, tb, aiPreliminary.GetAlarmzeit());
            PutOut(file_writer, tb, aiPreliminary.GetEinsatznr());
            PutOut(file_writer, tb, aiPreliminary.GetStrasse());
            PutOut(file_writer, tb, aiPreliminary.GetHausnr());
            PutOut(file_writer, tb, aiPreliminary.GetStrasseHausnr());
            PutOut(file_writer, tb, aiPreliminary.GetAbschnitt());
            PutOut(file_writer, tb, aiPreliminary.GetOrt());
            PutOut(file_writer, tb, aiPreliminary.GetOrtsteil());
            PutOut(file_writer, tb, aiPreliminary.GetGemeinde());
            PutOut(file_writer, tb, aiPreliminary.GetEinsatzort());
            PutOut(file_writer, tb, aiPreliminary.GetPlz());
            PutOut(file_writer, tb, aiPreliminary.GetSchlagwort());
            PutOut(file_writer, tb, aiPreliminary.GetStichwort());
            PutOut(file_writer, tb, aiPreliminary.GetEinsatzmittel());
            PutOut(file_writer, tb, aiPreliminary.GetFeuerwehren());
            PutOut(file_writer, tb, aiPreliminary.GetBemerkung());
            PutOut(file_writer, tb, aiPreliminary.GetAdresse());
            PutOut(file_writer, tb, aiPreliminary.GetObjekt());
            PutOut(file_writer, tb, aiPreliminary.GetKoordinateX());
            PutOut(file_writer, tb, aiPreliminary.GetKoordinateY());
            PutOut(file_writer, tb, aiPreliminary.GetKoordinaten());
            file_writer.Close();
            file_writer.Dispose();
            File.Move(filename_tempor, filename_destin);
            if (CanPushToWeb)
            {
                Push2Web(aiPreliminary, PhpScript);
            }
            return true;
        }

        public void PrintPdf(int numberOfCopies = 1)
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.CreateNoWindow = false;
            proc.StartInfo.Verb = "print";
            proc.StartInfo.FileName = pathFilenameArchive;
            proc.Start();
            proc.WaitForExit(10000);
            proc.Close();
        }

        private string UriEncode(string raw) 
        {
            raw = raw.Replace("ß", "%DF");
            raw = raw.Replace("ä", "%E4");
            raw = raw.Replace("ö", "%F6");
            raw = raw.Replace("ü", "%FC");
            raw = raw.Replace("Ä", "%C4");
            raw = raw.Replace("Ö", "%D6");
            raw = raw.Replace("Ü", "%DC");
            raw = raw.Replace(" ", "%20");
            raw = raw.Replace("(", "%28");
            raw = raw.Replace(")", "%29");
            raw = raw.Replace(">", "%3E");
            raw = raw.Replace("<", "%3C");
            return raw;
        }
        public void Push2Web(Alarmfaxinhalt ai, string phpScript)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            try
            {
                string url;
                url = phpScript;
                url += "alarmzeit=" + ai.GetAlarmzeit(true) + "&";
                url += "stichwort=" + ai.GetStichwort(true) + "&";
                url += "einsatzort=" + ai.GetOrtsteil(true) + "&";
                url += "schlagwort=" + ai.GetSchlagwort(true) + "&";
                url += "feuerwehren=" + ai.GetFeuerwehren(true);
                url = this.UriEncode(url);
                Console.WriteLine(url);
                byte[] myDataBuffer = wc.DownloadData(url);
                // Display the downloaded data.
                string download = Encoding.ASCII.GetString(myDataBuffer);
                Console.WriteLine(download);
            }
            catch (Exception ex)
            {
                // no error handling, just to keep software runnung
                Console.WriteLine(ex.Message.ToString());
            }
            wc.Dispose();
        }
    }
}
