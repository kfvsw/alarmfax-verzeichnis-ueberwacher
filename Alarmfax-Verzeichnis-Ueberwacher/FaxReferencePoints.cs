﻿/*
    Copyright (C) 2020 Ralf Weippert - All Rights Reserved    

    This file is part of Alarmfax-Verzeichnis-Überwacher.

    Alarmfax-Verzeichnis-Überwacher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Alarmfax-Verzeichnis-Überwacher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alarmfax-Verzeichnis-Überwacher.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Alarmfax-Verzeichnis-Überwacher.

    Das Programm Alarmfax-Verzeichnis-Überwacher ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Das Programm Alarmfax-Verzeichnis-Überwacher wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace WindowsFormsApplication1
{
    enum VerticalDirection { Up, Down };
    enum HorizontalDirection { Left, Right };

    class FaxReferencePoints
    {
        private int LowestReferenceRow;
        private int LeftBorder;
        private Bitmap FaxBitmap;

        public FaxReferencePoints(Bitmap faxBitmap)
        {
            FaxBitmap = faxBitmap;
            this.DetectLowestRefernceRow();
            this.DetectLeftBorder();
        }

        public int GetLowestReferenceRow()
        {
            return this.LowestReferenceRow;
        }

        public int GetLeftBorder()
        {
            return this.LeftBorder;
        }

        private void DetectLowestRefernceRow()
        {
            LowestReferenceRow = FindNextPixelRow(0, FaxBitmap.Height -1, 1, VerticalDirection.Up);
        }

        private void DetectLeftBorder()
        {
            int currentRow = this.LowestReferenceRow;
            currentRow = FindNextPixelRow(255, currentRow, 1, VerticalDirection.Up);
            for(int i = 0; i<3; i++)
            {
                currentRow = FindNextPixelRow(0, currentRow, 1, VerticalDirection.Up);
                currentRow = FindNextPixelRow(255, currentRow, 1, VerticalDirection.Up);
            }
            this.LeftBorder = FindNextPixelColumn(0, 1, currentRow, HorizontalDirection.Left);    
        }

        private int FindNextPixelColumn(int targetValue, int startColumn, int row, HorizontalDirection direction)
        {
            int currentPoint;
            bool endReached = true;
            int offset = 1;
            int endPoint = 0;

            if (row < 0) return -1;

            if (startColumn < 0)
            {
                return -1;
            }

            switch (direction)
            {
                case HorizontalDirection.Right:
                    offset = -1;
                    endPoint = 0;
                    break;
                case HorizontalDirection.Left:
                    offset = 1;
                    endPoint = this.FaxBitmap.Width - 1;
                    break;
            }

            for (currentPoint = startColumn; currentPoint != endPoint; currentPoint += offset)
            {
                if (this.FaxBitmap.GetPixel(currentPoint, row).B == targetValue)
                {
                    endReached = false;
                    break;
                }
            }
            if (endReached)
            {
                // targetValue not detected, return -1
                currentPoint = -1;
            }
            return currentPoint;
        }

        private int FindNextPixelRow(int targetValue, int startRow, int column, VerticalDirection direction)
        {
            int currentPoint;
            bool endReached = true;
            int offset = 1;
            int endPoint = 0;

            if(startRow < 0)
            {
                return -1;
            }

            switch (direction)
            {
                case VerticalDirection.Up:
                    offset = -1;
                    endPoint = 0;
                    break;
                case VerticalDirection.Down:
                    offset = 1;
                    endPoint = this.FaxBitmap.Height - 1;
                    break;
            }

            for (currentPoint = startRow; currentPoint != endPoint; currentPoint += offset)
            {
                if (this.FaxBitmap.GetPixel(column, currentPoint).B == targetValue)
                {
                    endReached = false;
                    break;
                }
            }
            if(endReached)
            {
                // targetValue not detected, return -1
                currentPoint = -1;
            }
            return currentPoint;
        }
    }
}
