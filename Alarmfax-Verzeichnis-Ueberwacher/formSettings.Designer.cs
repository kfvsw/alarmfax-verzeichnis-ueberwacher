﻿/*
    Copyright (C) 2020 Ralf Weippert - All Rights Reserved    

    This file is part of Alarmfax-Verzeichnis-Überwacher.

    Alarmfax-Verzeichnis-Überwacher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Alarmfax-Verzeichnis-Überwacher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alarmfax-Verzeichnis-Überwacher.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Alarmfax-Verzeichnis-Überwacher.

    Das Programm Alarmfax-Verzeichnis-Überwacher ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Das Programm Alarmfax-Verzeichnis-Überwacher wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

namespace WindowsFormsApplication1
{
    partial class formSettings
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmdChangeTesseract = new System.Windows.Forms.Button();
            this.labTesseractFolder = new System.Windows.Forms.Label();
            this.cmdChangeArchive = new System.Windows.Forms.Button();
            this.labArchiveFolder = new System.Windows.Forms.Label();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.labFilter = new System.Windows.Forms.Label();
            this.cmdChangeTarget = new System.Windows.Forms.Button();
            this.cmdChangeSource = new System.Windows.Forms.Button();
            this.labTargetFolder = new System.Windows.Forms.Label();
            this.labSourceFolder = new System.Windows.Forms.Label();
            this.chkWeb = new System.Windows.Forms.CheckBox();
            this.chkPrint = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.labDelay1 = new System.Windows.Forms.Label();
            this.txtDelay1 = new System.Windows.Forms.TextBox();
            this.labDelay2 = new System.Windows.Forms.Label();
            this.txtDelay2 = new System.Windows.Forms.TextBox();
            this.labDelay3 = new System.Windows.Forms.Label();
            this.txtDelay3 = new System.Windows.Forms.TextBox();
            this.toolTipDelay = new System.Windows.Forms.ToolTip(this.components);
            this.labRecipient = new System.Windows.Forms.Label();
            this.txtRecipient = new System.Windows.Forms.TextBox();
            this.txtPhpScript = new System.Windows.Forms.TextBox();
            this.labFailedFax = new System.Windows.Forms.Label();
            this.cmdChangeFailed = new System.Windows.Forms.Button();
            this.txtRecipientProvide = new System.Windows.Forms.TextBox();
            this.labRecipientProvide = new System.Windows.Forms.Label();
            this.cmdChangeTarget2 = new System.Windows.Forms.Button();
            this.labTargetFolder2 = new System.Windows.Forms.Label();
            this.txtSourceFolder = new System.Windows.Forms.TextBox();
            this.txtTargetFolder = new System.Windows.Forms.TextBox();
            this.txtArchiveFolder = new System.Windows.Forms.TextBox();
            this.txtTargetFolder2 = new System.Windows.Forms.TextBox();
            this.txtFailedFax = new System.Windows.Forms.TextBox();
            this.txtTesseractFolder = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmdChangeTesseract
            // 
            this.cmdChangeTesseract.Location = new System.Drawing.Point(800, 263);
            this.cmdChangeTesseract.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdChangeTesseract.Name = "cmdChangeTesseract";
            this.cmdChangeTesseract.Size = new System.Drawing.Size(112, 35);
            this.cmdChangeTesseract.TabIndex = 25;
            this.cmdChangeTesseract.Text = "ändern";
            this.cmdChangeTesseract.UseVisualStyleBackColor = true;
            this.cmdChangeTesseract.Click += new System.EventHandler(this.CmdChangeTesseract_Click);
            // 
            // labTesseractFolder
            // 
            this.labTesseractFolder.AutoSize = true;
            this.labTesseractFolder.Location = new System.Drawing.Point(12, 270);
            this.labTesseractFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTesseractFolder.Name = "labTesseractFolder";
            this.labTesseractFolder.Size = new System.Drawing.Size(169, 20);
            this.labTesseractFolder.TabIndex = 24;
            this.labTesseractFolder.Text = "Verzeichnis Tesseract:";
            // 
            // cmdChangeArchive
            // 
            this.cmdChangeArchive.Location = new System.Drawing.Point(800, 163);
            this.cmdChangeArchive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdChangeArchive.Name = "cmdChangeArchive";
            this.cmdChangeArchive.Size = new System.Drawing.Size(112, 35);
            this.cmdChangeArchive.TabIndex = 23;
            this.cmdChangeArchive.Text = "ändern";
            this.cmdChangeArchive.UseVisualStyleBackColor = true;
            this.cmdChangeArchive.Click += new System.EventHandler(this.CmdChangeArchive_Click);
            // 
            // labArchiveFolder
            // 
            this.labArchiveFolder.AutoSize = true;
            this.labArchiveFolder.Location = new System.Drawing.Point(12, 170);
            this.labArchiveFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labArchiveFolder.Name = "labArchiveFolder";
            this.labArchiveFolder.Size = new System.Drawing.Size(134, 20);
            this.labArchiveFolder.TabIndex = 22;
            this.labArchiveFolder.Text = "Archivverzeichnis:";
            // 
            // txtFilter
            // 
            this.txtFilter.Location = new System.Drawing.Point(265, 317);
            this.txtFilter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(500, 26);
            this.txtFilter.TabIndex = 21;
            this.txtFilter.Text = "*.pdf";
            // 
            // labFilter
            // 
            this.labFilter.AutoSize = true;
            this.labFilter.Location = new System.Drawing.Point(12, 320);
            this.labFilter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labFilter.Name = "labFilter";
            this.labFilter.Size = new System.Drawing.Size(167, 20);
            this.labFilter.TabIndex = 20;
            this.labFilter.Text = "Filter (Trennzeichen | )";
            // 
            // cmdChangeTarget
            // 
            this.cmdChangeTarget.Location = new System.Drawing.Point(800, 63);
            this.cmdChangeTarget.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdChangeTarget.Name = "cmdChangeTarget";
            this.cmdChangeTarget.Size = new System.Drawing.Size(112, 35);
            this.cmdChangeTarget.TabIndex = 19;
            this.cmdChangeTarget.Text = "ändern";
            this.cmdChangeTarget.UseVisualStyleBackColor = true;
            this.cmdChangeTarget.Click += new System.EventHandler(this.CmdChangeTarget_Click);
            // 
            // cmdChangeSource
            // 
            this.cmdChangeSource.Location = new System.Drawing.Point(800, 13);
            this.cmdChangeSource.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdChangeSource.Name = "cmdChangeSource";
            this.cmdChangeSource.Size = new System.Drawing.Size(112, 35);
            this.cmdChangeSource.TabIndex = 18;
            this.cmdChangeSource.Text = "ändern";
            this.cmdChangeSource.UseVisualStyleBackColor = true;
            this.cmdChangeSource.Click += new System.EventHandler(this.CmdChangeSource_Click);
            // 
            // labTargetFolder
            // 
            this.labTargetFolder.AutoSize = true;
            this.labTargetFolder.Location = new System.Drawing.Point(12, 70);
            this.labTargetFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTargetFolder.Name = "labTargetFolder";
            this.labTargetFolder.Size = new System.Drawing.Size(135, 20);
            this.labTargetFolder.TabIndex = 17;
            this.labTargetFolder.Text = "Zielverzeichnis(1):";
            // 
            // labSourceFolder
            // 
            this.labSourceFolder.AutoSize = true;
            this.labSourceFolder.Location = new System.Drawing.Point(12, 20);
            this.labSourceFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labSourceFolder.Name = "labSourceFolder";
            this.labSourceFolder.Size = new System.Drawing.Size(190, 20);
            this.labSourceFolder.TabIndex = 16;
            this.labSourceFolder.Text = "überwachtes Verzeichnis:";
            // 
            // chkWeb
            // 
            this.chkWeb.AutoSize = true;
            this.chkWeb.Location = new System.Drawing.Point(16, 420);
            this.chkWeb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkWeb.Name = "chkWeb";
            this.chkWeb.Size = new System.Drawing.Size(337, 24);
            this.chkWeb.TabIndex = 27;
            this.chkWeb.Text = "eingehendes Fax automatisch auf Website";
            this.chkWeb.UseVisualStyleBackColor = true;
            // 
            // chkPrint
            // 
            this.chkPrint.AutoSize = true;
            this.chkPrint.Location = new System.Drawing.Point(16, 370);
            this.chkPrint.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(309, 24);
            this.chkPrint.TabIndex = 26;
            this.chkPrint.Text = "eingehendes Fax automatisch drucken";
            this.chkPrint.UseVisualStyleBackColor = true;
            // 
            // labDelay1
            // 
            this.labDelay1.AutoSize = true;
            this.labDelay1.Location = new System.Drawing.Point(12, 470);
            this.labDelay1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labDelay1.Name = "labDelay1";
            this.labDelay1.Size = new System.Drawing.Size(259, 20);
            this.labDelay1.TabIndex = 29;
            this.labDelay1.Text = "Verzögerung nach Erkennung [ms]:";
            // 
            // txtDelay1
            // 
            this.txtDelay1.Location = new System.Drawing.Point(295, 470);
            this.txtDelay1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDelay1.Name = "txtDelay1";
            this.txtDelay1.Size = new System.Drawing.Size(148, 26);
            this.txtDelay1.TabIndex = 30;
            this.txtDelay1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextDelay1_KeyDown);
            // 
            // labDelay2
            // 
            this.labDelay2.AutoSize = true;
            this.labDelay2.Location = new System.Drawing.Point(12, 520);
            this.labDelay2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labDelay2.Name = "labDelay2";
            this.labDelay2.Size = new System.Drawing.Size(271, 20);
            this.labDelay2.TabIndex = 31;
            this.labDelay2.Text = "Zeit gleichbleibende Dateigröße [ms]:";
            // 
            // txtDelay2
            // 
            this.txtDelay2.Location = new System.Drawing.Point(295, 517);
            this.txtDelay2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDelay2.Name = "txtDelay2";
            this.txtDelay2.Size = new System.Drawing.Size(148, 26);
            this.txtDelay2.TabIndex = 32;
            this.txtDelay2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextDelay1_KeyDown);
            // 
            // labDelay3
            // 
            this.labDelay3.AutoSize = true;
            this.labDelay3.Location = new System.Drawing.Point(12, 570);
            this.labDelay3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labDelay3.Name = "labDelay3";
            this.labDelay3.Size = new System.Drawing.Size(116, 20);
            this.labDelay3.TabIndex = 33;
            this.labDelay3.Text = "Zykluszeit [ms]:";
            // 
            // txtDelay3
            // 
            this.txtDelay3.Location = new System.Drawing.Point(295, 567);
            this.txtDelay3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDelay3.Name = "txtDelay3";
            this.txtDelay3.Size = new System.Drawing.Size(148, 26);
            this.txtDelay3.TabIndex = 34;
            this.txtDelay3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextDelay1_KeyDown);
            // 
            // labRecipient
            // 
            this.labRecipient.AutoSize = true;
            this.labRecipient.Location = new System.Drawing.Point(12, 620);
            this.labRecipient.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labRecipient.Name = "labRecipient";
            this.labRecipient.Size = new System.Drawing.Size(222, 20);
            this.labRecipient.TabIndex = 35;
            this.labRecipient.Text = "Bei Absturz E-Mail senden an:";
            // 
            // txtRecipient
            // 
            this.txtRecipient.Location = new System.Drawing.Point(265, 617);
            this.txtRecipient.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRecipient.Name = "txtRecipient";
            this.txtRecipient.Size = new System.Drawing.Size(500, 26);
            this.txtRecipient.TabIndex = 36;
            // 
            // txtPhpScript
            // 
            this.txtPhpScript.Location = new System.Drawing.Point(361, 418);
            this.txtPhpScript.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPhpScript.Name = "txtPhpScript";
            this.txtPhpScript.Size = new System.Drawing.Size(404, 26);
            this.txtPhpScript.TabIndex = 37;
            this.txtPhpScript.TextChanged += new System.EventHandler(this.TxtPhpScript_TextChanged);
            // 
            // labFailedFax
            // 
            this.labFailedFax.AutoSize = true;
            this.labFailedFax.Location = new System.Drawing.Point(12, 220);
            this.labFailedFax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labFailedFax.Name = "labFailedFax";
            this.labFailedFax.Size = new System.Drawing.Size(237, 20);
            this.labFailedFax.TabIndex = 38;
            this.labFailedFax.Text = "Verzeichnis fehlgeschlage Faxe:";
            // 
            // cmdChangeFailed
            // 
            this.cmdChangeFailed.Location = new System.Drawing.Point(800, 213);
            this.cmdChangeFailed.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdChangeFailed.Name = "cmdChangeFailed";
            this.cmdChangeFailed.Size = new System.Drawing.Size(112, 35);
            this.cmdChangeFailed.TabIndex = 39;
            this.cmdChangeFailed.Text = "ändern";
            this.cmdChangeFailed.UseVisualStyleBackColor = true;
            this.cmdChangeFailed.Click += new System.EventHandler(this.CmdChangeFailed_Click);
            // 
            // txtRecipientProvide
            // 
            this.txtRecipientProvide.Location = new System.Drawing.Point(265, 667);
            this.txtRecipientProvide.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRecipientProvide.Name = "txtRecipientProvide";
            this.txtRecipientProvide.Size = new System.Drawing.Size(500, 26);
            this.txtRecipientProvide.TabIndex = 41;
            // 
            // labRecipientProvide
            // 
            this.labRecipientProvide.AutoSize = true;
            this.labRecipientProvide.Location = new System.Drawing.Point(12, 670);
            this.labRecipientProvide.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labRecipientProvide.Name = "labRecipientProvide";
            this.labRecipientProvide.Size = new System.Drawing.Size(141, 20);
            this.labRecipientProvide.TabIndex = 40;
            this.labRecipientProvide.Text = "Fax weiterleiten an";
            // 
            // cmdChangeTarget2
            // 
            this.cmdChangeTarget2.Location = new System.Drawing.Point(800, 113);
            this.cmdChangeTarget2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdChangeTarget2.Name = "cmdChangeTarget2";
            this.cmdChangeTarget2.Size = new System.Drawing.Size(112, 35);
            this.cmdChangeTarget2.TabIndex = 45;
            this.cmdChangeTarget2.Text = "ändern";
            this.cmdChangeTarget2.UseVisualStyleBackColor = true;
            this.cmdChangeTarget2.Click += new System.EventHandler(this.CmdChangeTarget2_Click);
            // 
            // labTargetFolder2
            // 
            this.labTargetFolder2.AutoSize = true;
            this.labTargetFolder2.Location = new System.Drawing.Point(12, 120);
            this.labTargetFolder2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTargetFolder2.Name = "labTargetFolder2";
            this.labTargetFolder2.Size = new System.Drawing.Size(135, 20);
            this.labTargetFolder2.TabIndex = 44;
            this.labTargetFolder2.Text = "Zielverzeichnis(2):";
            // 
            // txtSourceFolder
            // 
            this.txtSourceFolder.Location = new System.Drawing.Point(265, 17);
            this.txtSourceFolder.Name = "txtSourceFolder";
            this.txtSourceFolder.Size = new System.Drawing.Size(500, 26);
            this.txtSourceFolder.TabIndex = 46;
            // 
            // txtTargetFolder
            // 
            this.txtTargetFolder.Location = new System.Drawing.Point(265, 67);
            this.txtTargetFolder.Name = "txtTargetFolder";
            this.txtTargetFolder.Size = new System.Drawing.Size(500, 26);
            this.txtTargetFolder.TabIndex = 47;
            // 
            // txtArchiveFolder
            // 
            this.txtArchiveFolder.Location = new System.Drawing.Point(265, 167);
            this.txtArchiveFolder.Name = "txtArchiveFolder";
            this.txtArchiveFolder.Size = new System.Drawing.Size(500, 26);
            this.txtArchiveFolder.TabIndex = 48;
            // 
            // txtTargetFolder2
            // 
            this.txtTargetFolder2.Location = new System.Drawing.Point(265, 117);
            this.txtTargetFolder2.Name = "txtTargetFolder2";
            this.txtTargetFolder2.Size = new System.Drawing.Size(500, 26);
            this.txtTargetFolder2.TabIndex = 49;
            // 
            // txtFailedFax
            // 
            this.txtFailedFax.Location = new System.Drawing.Point(265, 217);
            this.txtFailedFax.Name = "txtFailedFax";
            this.txtFailedFax.Size = new System.Drawing.Size(500, 26);
            this.txtFailedFax.TabIndex = 50;
            // 
            // txtTesseractFolder
            // 
            this.txtTesseractFolder.Location = new System.Drawing.Point(265, 267);
            this.txtTesseractFolder.Name = "txtTesseractFolder";
            this.txtTesseractFolder.Size = new System.Drawing.Size(500, 26);
            this.txtTesseractFolder.TabIndex = 51;
            // 
            // formSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 713);
            this.Controls.Add(this.txtTesseractFolder);
            this.Controls.Add(this.txtFailedFax);
            this.Controls.Add(this.txtTargetFolder2);
            this.Controls.Add(this.txtArchiveFolder);
            this.Controls.Add(this.txtTargetFolder);
            this.Controls.Add(this.txtSourceFolder);
            this.Controls.Add(this.cmdChangeTarget2);
            this.Controls.Add(this.labTargetFolder2);
            this.Controls.Add(this.txtRecipientProvide);
            this.Controls.Add(this.labRecipientProvide);
            this.Controls.Add(this.cmdChangeFailed);
            this.Controls.Add(this.labFailedFax);
            this.Controls.Add(this.txtPhpScript);
            this.Controls.Add(this.txtRecipient);
            this.Controls.Add(this.labRecipient);
            this.Controls.Add(this.txtDelay3);
            this.Controls.Add(this.labDelay3);
            this.Controls.Add(this.txtDelay2);
            this.Controls.Add(this.labDelay2);
            this.Controls.Add(this.txtDelay1);
            this.Controls.Add(this.labDelay1);
            this.Controls.Add(this.chkWeb);
            this.Controls.Add(this.chkPrint);
            this.Controls.Add(this.cmdChangeTesseract);
            this.Controls.Add(this.labTesseractFolder);
            this.Controls.Add(this.cmdChangeArchive);
            this.Controls.Add(this.labArchiveFolder);
            this.Controls.Add(this.txtFilter);
            this.Controls.Add(this.labFilter);
            this.Controls.Add(this.cmdChangeTarget);
            this.Controls.Add(this.cmdChangeSource);
            this.Controls.Add(this.labTargetFolder);
            this.Controls.Add(this.labSourceFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Alarmfax-Überwacher Einstellungen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSettings_FormClosing);
            this.Load += new System.EventHandler(this.FormSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdChangeTesseract;
        private System.Windows.Forms.Label labTesseractFolder;
        private System.Windows.Forms.Button cmdChangeArchive;
        private System.Windows.Forms.Label labArchiveFolder;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Label labFilter;
        private System.Windows.Forms.Button cmdChangeTarget;
        private System.Windows.Forms.Button cmdChangeSource;
        private System.Windows.Forms.Label labTargetFolder;
        private System.Windows.Forms.Label labSourceFolder;
        private System.Windows.Forms.CheckBox chkWeb;
        private System.Windows.Forms.CheckBox chkPrint;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Label labDelay1;
        private System.Windows.Forms.TextBox txtDelay1;
        private System.Windows.Forms.Label labDelay2;
        private System.Windows.Forms.TextBox txtDelay2;
        private System.Windows.Forms.Label labDelay3;
        private System.Windows.Forms.TextBox txtDelay3;
        private System.Windows.Forms.ToolTip toolTipDelay;
        private System.Windows.Forms.Label labRecipient;
        private System.Windows.Forms.TextBox txtRecipient;
        private System.Windows.Forms.TextBox txtPhpScript;
        private System.Windows.Forms.Label labFailedFax;
        private System.Windows.Forms.Button cmdChangeFailed;
        private System.Windows.Forms.TextBox txtRecipientProvide;
        private System.Windows.Forms.Label labRecipientProvide;
        private System.Windows.Forms.Button cmdChangeTarget2;
        private System.Windows.Forms.Label labTargetFolder2;
        private System.Windows.Forms.TextBox txtSourceFolder;
        private System.Windows.Forms.TextBox txtTargetFolder;
        private System.Windows.Forms.TextBox txtArchiveFolder;
        private System.Windows.Forms.TextBox txtTargetFolder2;
        private System.Windows.Forms.TextBox txtFailedFax;
        private System.Windows.Forms.TextBox txtTesseractFolder;
    }
}