﻿/*
    Copyright (C) 2020 Ralf Weippert - All Rights Reserved    

    This file is part of Alarmfax-Verzeichnis-Überwacher.

    Alarmfax-Verzeichnis-Überwacher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Alarmfax-Verzeichnis-Überwacher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alarmfax-Verzeichnis-Überwacher.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Alarmfax-Verzeichnis-Überwacher.

    Das Programm Alarmfax-Verzeichnis-Überwacher ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Das Programm Alarmfax-Verzeichnis-Überwacher wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

public class SettingsRA
{
    public string pathTarget1 = "";                                         // target folder #1 for fax files
    public string pathSource = "";                                          // source folder for fax files
    public string pathArchive = "";                                         // archive folder
    public string pathTarget2 = "";                                         // target folder #2 for fax files
    public string pathFailed = "";                                          // folder for files which processing was not possible
    public string pathTesseract = System.IO.Directory.GetCurrentDirectory() + @"\Tesseract-OCR\"; // folder tesseract
    public string filter = "";                                              // Dateitypfilter
    public bool automaticPrint = false;                                     // eingehende Faxe automatisch drucken
    public bool automaticSend2Web = false;                                  // eingehende Faxe automatisch auf Webseite senden
    public Int32 delay1 = 0;                                                // Verzoegerung, nachdem Datei erkannt wurde
    public Int32 delay2 = 0;                                                // Zeitspanne, in der die Dateigröße sich nicht mehr aendern darf
    public Int32 delay3 = 200;                                              // Zykluszeit, in der nach neuen Dateien gesucht wird
    public string recipient = "";                                           // Empfaenger bei Absturz
    public string recipientProvide = "";                                    // Empfaneger der Faxe
    public string PhpScript = "";                                           // PHP-Script fuer automatischen Webeintrag
    public bool NoEmail = true;                                             // if set to true, e-mail provision is stopped
    public int FontSize = 100;                                              // font size %

    private string pathApplicationData = "";

    public SettingsRA()
    {
        string pathApplicationDataFolder;
        pathApplicationDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Alarmfax-Verzeichnis-Ueberwacher";
        pathApplicationData = pathApplicationDataFolder + @"\settings.xml";
        if (!Directory.Exists(pathApplicationDataFolder))
        {
            Directory.CreateDirectory(pathApplicationDataFolder);
        }

        if (File.Exists(pathApplicationData))
        {
            XmlReader reader = XmlReader.Create(pathApplicationData);
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element && (reader.Name == "Einstellungen" || reader.Name == "Settings"))
                {
                    while (reader.NodeType != XmlNodeType.EndElement)
                    {
                        reader.Read();
                        if (reader.Name == "TargetPath")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.pathTarget1 = reader.Value;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "SourcePath")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.pathSource = reader.Value;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "ArchivePath")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.pathArchive = reader.Value;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "TargetPath2")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.pathTarget2 = reader.Value;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "FailedPath")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.pathFailed = reader.Value;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "Filter")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.filter = reader.Value;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "TesseractPath")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.pathTesseract = reader.Value;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "AutomaticPrint")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.automaticPrint = reader.Value == "1" ? true : false;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "AutomaticSend2Web")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.automaticSend2Web = reader.Value == "1" ? true : false;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "Delay1")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.delay1 = Int32.Parse(reader.Value);
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "Delay2")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.delay2 = Int32.Parse(reader.Value);
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "Delay3")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.delay3 = Int32.Parse(reader.Value);
                                }
                            }
                            reader.Read();
                        }

                        if (reader.Name == "PhpScript")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.PhpScript = reader.Value;
                                }
                            }
                            reader.Read();
                        }

                        if (reader.Name == "Recipient")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.recipient = reader.Value;
                                }
                            }
                            reader.Read();
                        }

                        if (reader.Name == "RecipientProvide")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.recipientProvide = reader.Value;
                                }
                            }
                            reader.Read();
                        }

                        if (reader.Name == "NoEmail")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.NoEmail = reader.Value == "1" ? true : false;
                                }
                            }
                            reader.Read();
                        }
                        if (reader.Name == "FontSize")
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                reader.Read();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    this.FontSize = int.Parse(reader.Value);
                                }
                            }
                            reader.Read();
                        }

                    }
                }
            }
            reader.Close();
        }
    }

    public void WriteApplicationSettings()
    {
        //TODO: Error handling; what happens, if writing is not possible?
        XmlWriterSettings settings = new XmlWriterSettings
        {
            Indent = true
        };
        XmlWriter writer = XmlWriter.Create(pathApplicationData, settings);
        writer.WriteStartDocument();
        writer.WriteStartElement("Settings");
        WriteSetting(writer, "TargetPath", this.pathTarget1);
        WriteSetting(writer, "SourcePath", this.pathSource);
        WriteSetting(writer, "ArchivePath", this.pathArchive);
        WriteSetting(writer, "TargetPath2", this.pathTarget2);
        WriteSetting(writer, "FailedPath", this.pathFailed);
        WriteSetting(writer, "Filter", this.filter);
        WriteSetting(writer, "TesseractPath", this.pathTesseract);
        WriteSetting(writer, "AutomaticPrint", this.automaticPrint);
        WriteSetting(writer, "AutomaticSend2Web", this.automaticSend2Web);
        WriteSetting(writer, "Delay1", this.delay1);
        WriteSetting(writer, "Delay2", this.delay2);
        WriteSetting(writer, "Delay3", this.delay3);
        WriteSetting(writer, "Recipient", this.recipient);
        WriteSetting(writer, "RecipientProvide", this.recipientProvide);
        WriteSetting(writer, "PhpScript", this.PhpScript);
        WriteSetting(writer, "NoEmail", this.NoEmail);
        WriteSetting(writer, "FontSize", this.FontSize);
        writer.WriteEndElement();
        writer.WriteEndDocument();
        writer.Flush();
        writer.Close();
    }

    private void WriteSetting(XmlWriter writer, string localName, bool value)
    {
        string tmp = value ? "1" : "0";
        writer.WriteElementString(localName, tmp);
    }
    private void WriteSetting(XmlWriter writer, string localName, string value)
    {
        if (value != "")
        {
            writer.WriteElementString(localName, value);
        }
    }
    private void WriteSetting(XmlWriter writer, string localName, int value)
    {
        writer.WriteElementString(localName, value.ToString());
    }
}