﻿/*
    Copyright (C) 2020 Ralf Weippert - All Rights Reserved    

    This file is part of Alarmfax-Verzeichnis-Überwacher.

    Alarmfax-Verzeichnis-Überwacher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Alarmfax-Verzeichnis-Überwacher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alarmfax-Verzeichnis-Überwacher.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Alarmfax-Verzeichnis-Überwacher.

    Das Programm Alarmfax-Verzeichnis-Überwacher ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Das Programm Alarmfax-Verzeichnis-Überwacher wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    public enum Delete {None, Single, AllBefore };
public class Alarmfaxinhalt
    {
        public const string ILS_SW = "ILS Schweinfurt";
        public const string ILS_WUE = "ILS Würzburg";
        public const string ILS_AM = "ILS AMBERG";
        public const string ILS_TS = "ILS Traunstein";
        private const int MAX_CONTENT_COUNT = 400;
        private bool withEquipment = false;
        protected string Absender;
        private string AbsenderFaxNr = "";
        private string AbsenderTelefonNr = "";
        protected string Alarmzeit = "";
        protected string EinsatzNr = "";
        protected string Strasse = "";
        protected string Hausnr = "";
        protected string Abschnitt = "";
        private string Ortsteil = "";
        protected string Ort = "";
        protected string KoordinateX = "";
        protected string KoordinateY = "";
        private string Plz = "";
        private string Adresse = "";
        protected string Schlagwort = "";
        protected string Stichwort = "";
        private string Einsatzmittel = "";
        private List<Einsatzmittel> EinsatzmittelSwe = new List<Einsatzmittel>();
        private string Feuerwehren = "";
        protected string Bemerkung = "";
        protected string Objekt = "";
        private string Fehlerspeicher = "";
        public List<string> Content = new List<string>();
        protected readonly char[] CharsToTrim = { '.', ' ', ':', '=' };

        public string CheckMinimumContentWords(string[] catchwords)
        {
            string errorText = "";
            foreach (string s in catchwords)
            {
                if (!Content.Exists(element => element.IndexOf(s) >= 0))
                {
                    errorText += "[" + s + "]";
                }
            }
            return errorText;
        }

        /// <summary>
        /// extracts multipe lines from attribute content and puts it together into one string; lines are separated by \n
        /// </summary>
        /// <param name="searchStringStart">search string for first line</param>
        /// <param name="searchStringEnd">search string for last line</param>
        /// <param name="delete">defines wether and what has to be deleted from content</param>
        /// <param name="atStartOfLine">defines if search strings have to be at the start of the line or not</param>
        /// <param name="isPayloadStartLine">defines if startline is payload or not</param>
        /// <param name="isPayloadEndLine">defines if endline is payload or not</param>
        /// <param name="isOptionalStringEnd">if set and end string has not been found, content up to the end is taken</param>
        /// <returns>single string of multiline content separted by \n</returns>
        public string ExtractMultiLineContent(string searchStringStart, string searchStringEnd, Delete delete = Delete.Single, bool atStartOfLine = false, bool isPayloadStartLine = false, bool isPayloadEndLine= false, bool isOptionalStringEnd = false)
        {
            int indexStart = -1;
            int indexStartPayload = -1;

            if (atStartOfLine)
            {
                indexStart = indexStartPayload = Content.FindIndex(element => element.IndexOf(searchStringStart) == 0);
            }
            else
            {
                indexStart = indexStartPayload = Content.FindIndex(element => element.IndexOf(searchStringStart) >= 0);
            }

            int indexEnd = -1;
            int indexEndPayload = -1;

            if (atStartOfLine)
            {
                indexEnd = indexEndPayload = Content.FindIndex(element => element.IndexOf(searchStringEnd) == 0);
            }
            else
            {
                indexEnd = indexEndPayload = Content.FindIndex(element => element.IndexOf(searchStringEnd) >= 0);
            }

            if(indexEnd < 0 && isOptionalStringEnd)
            {
                indexEnd = indexEndPayload = Content.Count - 1;
                isPayloadEndLine = true;
            }

            if (indexStart >= 0 && indexEnd >= 0)
            {
                if (!isPayloadStartLine) indexStartPayload++;
                if (!isPayloadEndLine) indexEndPayload--;
                string multiLineContent = "";

                if (indexStartPayload <= indexEndPayload)
                {
                    for (int i = indexStartPayload; i <= indexEndPayload; i++)
                    {
                        multiLineContent += Content[i].ToString() + "\n";
                    }
                    multiLineContent = multiLineContent.Substring(0, multiLineContent.Length - 1);
                }

                switch (delete)
                {
                    case Delete.Single:
                        Content.RemoveRange(indexStart, indexEnd - indexStart + 1);
                        break;
                    case Delete.AllBefore:
                        Content.RemoveRange(0, indexEnd + 1);
                        break;
                    default:
                        break;
                }
                return multiLineContent;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// extracts single line from attribute content and scramble this line into multiple results
        /// </summary>
        /// <param name="searchString">search string to find single line</param>
        /// <param name="delete">defines wether and what has to be deleted from content</param>
        /// <param name="atStartOfLine">defines if search strings have to be at the start of the line or not</param>
        /// <returns>array of results</returns>
        public string[] ExtractSingleLineMultiContent(List<string> searchString, Delete delete = Delete.Single, bool atStartOfLine = true)
        {
            int index = -1;

            if (atStartOfLine)
            {
                index = Content.FindIndex(element => element.IndexOf(searchString[0]) == 0);
            }
            else
            {
                index = Content.FindIndex(element => element.IndexOf(searchString[0]) >= 0);
            }

            if (index >= 0)
            {
                string lineContent = Content[index].ToString();
                switch (delete)
                {
                    case Delete.Single:
                        Content.RemoveAt(index);
                        break;
                    case Delete.AllBefore:
                        Content.RemoveRange(0, index + 1);
                        break;
                    default:
                        break;
                }

                string[] results = new string[searchString.Count()];
                for(int i = searchString.Count() - 1; i >= 0; i--)
                {
                    int pos = lineContent.IndexOf(searchString[i]);
                    int len = searchString[i].Length;
                    if (pos >= 0)
                    {
                        results[i] = lineContent.Substring(pos + len);
                        lineContent = lineContent.Substring(0, pos);
                    }
                }
                return results;
            }
            else
            {
                return null;
            }
        }

        public string ExtractSingleLineContent(string searchString, Delete delete = Delete.Single, bool atStartOfLine = true, bool searchStringDelete = false)
        {
            int index = -1;

            if (atStartOfLine)
            {
                index = Content.FindIndex(element => element.IndexOf(searchString) == 0);
            }
            else
            {
                index = Content.FindIndex(element => element.IndexOf(searchString) >= 0);
            }

            if (index >= 0)
            {
                string lineContent = Content[index].ToString();
                switch(delete)
                {
                    case Delete.Single:
                        Content.RemoveAt(index);
                        break;
                    case Delete.AllBefore:
                        Content.RemoveRange(0, index + 1);
                        break;
                    default:
                        break;
                }
                if(searchStringDelete)
                {
                    lineContent = lineContent.Replace(searchString, "");
                    lineContent = lineContent.Trim(CharsToTrim);
                }
                return lineContent;
            }
            else
            {
                return null;
            }
        }

        private string AddMarkup(string markup, string content)
        {
            return "<" + markup + ">" + content + "</" + markup + ">";
        }

        public void SetAbsenderFaxNr(string raw)
        {
            AbsenderFaxNr = raw.Trim();
        }

        public void SetAlarmzeit(string raw)
        {
            if (raw != null)
            {
                raw = raw.Replace("Zeit", "");
                raw = raw.Replace("Termin", "");
                raw = raw.Trim(CharsToTrim);
                Alarmzeit = raw;
            }
        }

        public void SetAbsender(string raw)
        {
            if (raw != null)
            {
                raw = raw.Replace("Absender", "");
                raw = raw.Trim(CharsToTrim);
                Absender = raw;
            }
        }

        public string GetAbsender(bool AddMarkup = true)
        {
            if(AddMarkup)
            {
                return this.AddMarkup("Absender", Absender);
            }
            else
            {
                return Absender;
            }
        }
        public bool IsIlsSw()
        {
            if (Absender == ILS_SW)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsIlsWue()
        {
            if (Absender == ILS_WUE)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsIlsAm()
        {
            if (Absender == ILS_AM)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsIlsTs()
        {
            if (Absender == ILS_TS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public string GetEinsatznr()
        {
            return AddMarkup("Einsatznr", EinsatzNr);
        }

        public void SetStrasse(string raw)
        {
            // check parameter         
            if (raw == "")
            {
                Fehlerspeicher += "\nsetStrasse: leerer String erhalten";
                return;
            }

            int posStrasse = raw.IndexOf("Strasse");
            int posHausNrKm = raw.IndexOf("Haus-Nr./Km");
            if (posStrasse < 0)
            {
                Fehlerspeicher += "\nsetStrasse: Das WOrt <Strasse> nicht gefunden";
                return;
            }
            if (posHausNrKm < 0 && this.IsIlsWue())
            {
                Fehlerspeicher += "\nsetStrasse: Das WOrt <Haus-Nr./Km> nicht gefunden";
            }


            raw = raw.Substring(posStrasse + 6);
            if (this.IsIlsWue())
            {
                posHausNrKm = raw.IndexOf("Haus-Nr./Km");
                raw = raw.Substring(0, posHausNrKm);
            }
            raw = raw.Trim(CharsToTrim);
            Strasse = raw;
        }
        public string GetAbsenderFaxNr()
        {
            return AddMarkup("AbsenderFaxNr", AbsenderFaxNr);
        }
        public string GetAlarmzeit(bool web = false)
        {
            if (web)
            {
                string webdate = "";
                webdate += Alarmzeit.Substring(6, 4) + "-";
                webdate += Alarmzeit.Substring(3, 2) + "-";
                webdate += Alarmzeit.Substring(0, 2) + " ";
                webdate += Alarmzeit.Substring(11, 2) + ":";
                webdate += Alarmzeit.Substring(14, 2) + ":";
                webdate += Alarmzeit.Substring(17, 2);
                return webdate;
            }
            else
            {
                return AddMarkup("Alarmzeit", Alarmzeit);
            }
        }
        public string GetStrasse()
        {
            return AddMarkup("Strasse", Strasse);
        }
        public void SetHausnr(string raw)
        {
            if (this.IsIlsSw())
            {
                raw = raw.Replace("Haus-Nr", "");
            }
            if (this.IsIlsWue())
            {
                raw = raw.Substring(raw.IndexOf("Haus-Nr./Km") + 11);
            }
            raw = raw.Trim(CharsToTrim);
            Hausnr = raw;
        }
        public string GetHausnr()
        {
            return AddMarkup("Hausnr", Hausnr);
        }
        public string GetStrasseHausnr()
        {
            return AddMarkup("StrasseHausnr", Strasse + " " + Hausnr);
        }
        public void SetAbschnitt(string raw)
        {
            if (raw != null)
            {
                raw = raw.Replace("Abschnitt", "");
                raw = raw.Trim(CharsToTrim);
                if (Strasse != raw)
                {
                    Abschnitt += raw;
                }
            }
        }
        public string GetAbschnitt()
        {
            return AddMarkup("Abschnitt", Abschnitt);
        }
        public void SetOrt1(string raw)
        {
            if (raw == null || raw == "")
            {
                Ort = "";
                return;
            }
            if (raw.IndexOf("-") >= 0)
            {
                Ort = raw.Substring(raw.IndexOf("-") + 1);
                Ort = Ort.Trim(CharsToTrim);
                Ortsteil = raw.Substring(0, raw.IndexOf("-"));
            }
            else
            {
                Ortsteil = raw;
            }
            Ortsteil = Ortsteil.Replace("Ortsteil", "");
            Ortsteil = Ortsteil.Trim(CharsToTrim);
        }
        public void SetOrt2(string raw)
        {
            if (raw == null || raw == "")
            {
                Ort = "";
                return;
            }
            int start;
            int index_blank;
            int index_first_redundancy;
            // check if there is PLZ and extract it
            Plz = raw.Substring(raw.IndexOf(":") + 1);
            Plz = Plz.TrimStart(CharsToTrim);
            Plz = Plz.Substring(0, 5);
            if (Plz.All(Char.IsDigit))
            {
                start = raw.IndexOf(Plz) + 6;
            }
            else
            {
                start = raw.IndexOf(":") + 1;
                Plz = "";
            }
            // check if there is Ortsteil and extract it
            if (raw.IndexOf("-", start) > 0)
            {
                Ortsteil = raw.Substring(start, raw.IndexOf("-", start) - start);
                Ort = raw.Substring(raw.IndexOf("-", start) + 1);
            }
            else
            {
                Ort = raw.Substring(start);
            }
            Ortsteil = Ortsteil.Trim(CharsToTrim);
            Ort = Ort.Trim(CharsToTrim);
            index_blank = Ort.IndexOf(" ");
            if (index_blank > 0)
            {
                index_first_redundancy = Ort.LastIndexOf(Ort.Substring(0, index_blank));
                if (index_first_redundancy > 0)
                {
                    Ort = Ort.Substring(0, index_first_redundancy - 1);
                    Ort = Ort.Trim(CharsToTrim);
                }
            }
        }

        public string GetOrt()
        {
            if (Ort != Ortsteil && Ortsteil != "")
            {
                return AddMarkup("Ort", Ortsteil + " - " + Ort);
            }
            else
            {
                return AddMarkup("Ort", Ort);
            }
        }
        public string GetGemeinde()
        {
            return AddMarkup("Gemeinde", Ort);
        }
        public string GetOrtsteil(bool web = false)
        {
            if (!web)
            {
                if (Ort != Ortsteil)
                {
                    return AddMarkup("Ortsteil", Ortsteil);
                }
                else
                {
                    return AddMarkup("Ortsteil", "");
                }
            }
            else
            {
                if (Ortsteil != "")
                {
                    return Ortsteil;
                }
                else
                {
                    return Ort;
                }
            }
        }
        public void SetPlz(string raw)
        {
            raw = raw.Replace("Ort", "");
            raw = raw.Trim(CharsToTrim);
            if (raw == "")
            {
                Plz = "";
                return;
            }
            if (raw.IndexOf(" ") == 5)
            {
                if (Ort == "")
                {
                    Ort = raw.Substring(6);
                }
                Plz = raw.Substring(0, 5);
            }
            else
            {
                Ort = raw;
            }
        }
        public string GetPlz()
        {
            return AddMarkup("PLZ", Plz);
        }
        public void SetKoordinate(string raw)
        {
            if (raw != null)
            {
                if (raw.IndexOf("WGS84") > 0)
                {
                    SetKoordinateWGS(raw);
                }
                else if (raw.IndexOf("Gauß-Krüger") > 0)
                {
                    SetKoordinateGaussKrueger(raw);
                }
            }
        }
        public void SetKoordinateWGS(string raw)
        {
            int posStart;
            int posEnd;
            posStart = raw.IndexOf(":") + 1;
            posEnd = raw.IndexOf("N", posStart + 1) - 1;
            if(posStart > 0 && posEnd > 0)
            {
                KoordinateX = raw.Substring(posStart, posEnd - posStart).Trim();
                posStart = posEnd + 4;
                posEnd = raw.IndexOf("E", posStart + 1) - 1;
                if(posEnd > 0)
                {
                    KoordinateY = raw.Substring(posStart, posEnd - posStart).Trim();
                }
            }
        }
        public void SetKoordinateGaussKrueger(string raw)
        {
            int posStart;
            int posEnd;
            char[] CharsToTrim2 = { '-' };
            posStart = raw.ToUpper().IndexOf("X");
            posEnd = raw.ToUpper().IndexOf("Y");
            if(posStart >= 0 && posEnd >= 0)
            {
                KoordinateX = raw.Substring(posStart + 1, posEnd - posStart - 1).Trim(CharsToTrim);
                posStart = posEnd + 1;
                posEnd = raw.ToUpper().IndexOf("SYSTEM");
                if (posEnd >= 0 && posEnd > posStart)
                {
                    KoordinateY = raw.Substring(posStart + 1, posEnd - posStart - 1).Trim(CharsToTrim).Trim(CharsToTrim2).Trim(CharsToTrim);
                }
            }
        }
        public string GetKoordinateX()
        {
            return AddMarkup("KoordinateX", KoordinateX);
        }
        public string GetKoordinateY()
        {
            return AddMarkup("KoordinateY", KoordinateY);
        }
        public string GetKoordinaten()
        {
            return AddMarkup("Koordinaten", KoordinateX + "," + KoordinateY);
        }
        public string GetObjekt()
        {
            return AddMarkup("Objekt", Objekt);
        }
        public string GetEinsatzort()
        {
            string temp = "";
            if (Objekt != "")
            {
                temp = temp + Objekt + "<zv>\n";
            }
            if (Strasse != "")
            {
                temp += Strasse;
                if (Hausnr != "")
                {
                    temp = temp + " " + Hausnr;
                }
                temp += "<zv>\n";
            }
            if (Plz != "")
            {
                temp = temp + Plz + " ";
            }
            temp += Ort;
            if (Ortsteil != "" && Ortsteil != Ort)
            {
                temp = temp + "<zv>\n" + Ortsteil + " - " + Ort;
            }
            if (Abschnitt != "")
            {
                temp = temp + "<zv>\n" + Abschnitt;
            }
            return AddMarkup("Einsatzort", temp);
        }
        public void SetObjekt(string raw)
        {
            raw = raw.Replace("Objekt", "");
            raw = raw.Trim(CharsToTrim);
            Objekt = raw;
        }
        public void SetSchlagwort(string raw)
        {
            if (IsIlsSw())
            {
                raw = raw.Replace("Schlagwort", "");
            }
            if (IsIlsWue())
            {
                raw = raw.Replace("Schlagw", "");
            }
            raw = raw.Trim(CharsToTrim);
            Schlagwort = raw;
        }
        public void SetWithEquipment(string raw)
        {
            if (raw.IndexOf("Gerät") > 0)
            {
                this.withEquipment = true;
            }
        }
        public string GetSchlagwort(bool web = false)
        {
            if (web)
            {
                return Schlagwort;
            }
            else
            {
                return AddMarkup("Schlagwort", Schlagwort);
            }
        }
        public void SetStichwort(string rawB, string rawT, string rawS, string rawI, string rawR)
        {
            rawB = rawB.Replace("StichwOrt B", "");
            rawB = rawB.Replace("Stichwort B", "");
            rawB = rawB.Trim(CharsToTrim);
            rawT = rawT.Replace("StichwOrt TH", "");
            rawT = rawT.Replace("StichwOrt T", "");
            rawT = rawT.Replace("Stichwort T", "");
            rawT = rawT.Trim(CharsToTrim);
            rawS = rawS.Replace("Stichwwort SO", "");
            rawS = rawS.Replace("StichwOrt S", "");
            rawS = rawS.Replace("Stichwort S", "");
            rawS = rawS.Trim(CharsToTrim);
            rawI = rawI.Replace("Stichwort IN", "");
            rawI = rawI.Replace("StichwOrt I", "");
            rawI = rawI.Replace("Stichwort I", "");
            rawI = rawI.Trim(CharsToTrim);
            rawR = rawR.Replace("Stichwort RD", "");
            rawR = rawR.Replace("StichwOrt R", "");
            rawR = rawR.Replace("Stichwort R", "");
            rawR = rawR.Trim(CharsToTrim);
            Stichwort = "";
            Stichwort = AddCommaSeparated(Stichwort, rawB);
            Stichwort = AddCommaSeparated(Stichwort, rawT);
            Stichwort = AddCommaSeparated(Stichwort, rawS);
            Stichwort = AddCommaSeparated(Stichwort, rawI);
            Stichwort = AddCommaSeparated(Stichwort, rawR);
        }
        public string GetAdresse()
        {
            if (Strasse != "" && Strasse.IndexOf(">") < 0)
            {
                Adresse = Strasse;
                if (Hausnr != "" && Hausnr != "0")
                {
                    Adresse = Strasse + " " + Hausnr;
                }
                Adresse += ", ";
            }
            if (Plz != "")
            {
                Adresse = Adresse + Plz + " ";
            }
            if (Ortsteil != "")
            {
                Adresse += Ortsteil;
            }
            else
            {
                Adresse += Ort;
            }
            Console.WriteLine(Adresse);
            return AddMarkup("Adresse", Adresse);
        }
        private string AddCommaSeparated(string list, string newElement)
        {
            if (newElement != "")
            {
                if (list == "")
                {
                    list = newElement;
                }
                else
                {
                    list = list + ", " + newElement;
                }
            }
            return list;
        }
        public string GetStichwort(bool web = false)
        {
            if (web)
            {
                return Stichwort;
            }
            else
            {
                return AddMarkup("Stichwort", Stichwort);
            }
        }
        public void SetEinsatzmittel1(string raw)
        {
            if (raw == "" || raw == null)
            {
                return;
            }
            if (raw.IndexOf("Name") >= 0 || raw.IndexOf("Einsatzmittelname") >= 0)
            {
                EinsatzmittelSwe.Add(new Einsatzmittel(raw));
            }
            if (raw.IndexOf("Ausstattung") >= 0 || raw.IndexOf("Gerät") >= 0 || raw.IndexOf("gef. Geräte") >= 0)
            {
                    EinsatzmittelSwe[EinsatzmittelSwe.Count - 1].AddEquipment(raw);
            }
            if (raw.IndexOf("Alarmiert") >= 0)
            {
                EinsatzmittelSwe[EinsatzmittelSwe.Count - 1].AddDispatchTime(raw);
            }
        }

        public void SetEinsatzmittel2(string raw)
        {
            string equipment;
            string unit;
            int index_first;
            int index_second = -1;
            if (raw == "" || raw == null)
            {
                return;
            }
            index_first = raw.IndexOf(":");
            if (index_first > 0)
            {
                index_second = raw.IndexOf(":", index_first + 1);
            }
            if (index_first > 0 && index_second > 0)
            {
                unit = raw.Substring(0, index_first);
                unit = unit.Trim(CharsToTrim);
                if (unit.Substring(unit.Length - 1) == "O" || unit.Substring(unit.Length - 1) == "Q")
                {
                    unit = unit.Substring(0, unit.Length - 1) + "@";
                }
                if (withEquipment)
                {
                    equipment = raw.Substring(index_first + 1, index_second - index_first);
                    equipment = equipment.Trim(CharsToTrim);
                    if (equipment != "")
                    {
                        equipment = " (" + equipment + ")";
                        unit += equipment;
                    }
                }
                if (Einsatzmittel != "")
                {
                    Einsatzmittel += "<zv>\n";
                }
                Einsatzmittel += unit;
            }
            else
            {
                // TODO: Zeile nicht als Einsatzmittel erkannt, was nun? ggf. Ausgabe in Fenster
            }
        }

        public string GetEinsatzmittel()
        {
            if (this.EinsatzmittelSwe.Count > 0)
            {
                foreach (Einsatzmittel em in EinsatzmittelSwe)
                {
                    if (Einsatzmittel != "") Einsatzmittel += "<zv>\n";
                    Einsatzmittel += em.GetDepartmentType(true).Replace("FL", "Florian").Replace("KAT", "Kater");
                    Einsatzmittel += em.GetName(false);
                    Einsatzmittel += em.GetRadioIndex(true);
                    Einsatzmittel += em.GetEquipment(true);
                }
            }
            return AddMarkup("Einsatzmittel", Einsatzmittel);
        }
        public string GetFeuerwehren(bool web = false)
        {
            string name_fire_dep;
            if (IsIlsSw())
            {
                Feuerwehren = "";
                foreach (Einsatzmittel em in EinsatzmittelSwe)
                {
                    if (em.GetDepartmentType(false) == "FF" || em.GetDepartmentType(false) == "FL" || em.GetDepartmentType(false) == "KAT")
                    {
                        name_fire_dep = em.GetName(false);
                        // special UGOEL
                        if (name_fire_dep == "Schweinfurt-Land" && em.GetRadioIndex(false) == "13/1")
                        {
                            if (Feuerwehren != "") Feuerwehren += ",";
                            Feuerwehren += "UGÖEL";
                        }

                        if (name_fire_dep != "Schweinfurt-Land")
                        {
                            // special Stadt Schweinfurt
                            if (name_fire_dep == "Schweinfurt")
                            {
                                name_fire_dep = "Stadt Schweinfurt";
                            }
                            if (Feuerwehren.IndexOf(name_fire_dep) < 0)
                            {
                                if (Feuerwehren != "") Feuerwehren += ",";
                                Feuerwehren += "FF " + name_fire_dep;
                                if (em.GetCounty() != "SW-L" && em.GetCounty() != "SW-S")
                                {
                                    Feuerwehren += " (" + em.GetCounty() + ")";
                                }
                            }
                        }
                    }
                }
            }
            if (web)
            {
                return Feuerwehren;
            }
            else
            {
                return AddMarkup("Feuerwehren", Feuerwehren);
            }
        }
        public void SetAbsenderTelefonNr(string raw)
        {
            raw = raw.Replace("Telefon", "");
            raw = raw.Trim(CharsToTrim);
            AbsenderTelefonNr = raw;
        }
        public void SetBemerkung(string raw)
        {
            if (Bemerkung != "")
            {
                Bemerkung += "\n";
            }
            Bemerkung += raw;
        }
        public string GetBemerkung()
        {
            return AddMarkup("Bemerkung", Bemerkung);
        }
    }

    /// <summary>
    /// Class for Alarmfaxinhalt ILS Schweinfurt
    /// </summary>
    class AlarmfaxinhaltIlsSw : Alarmfaxinhalt
    {
        public AlarmfaxinhaltIlsSw(List<string> content, string absender)
        {
            this.Content = content;
            this.Absender = absender;
        }

        public void ParseContent()
        {
            int counter = 0;
            this.Content.RemoveAt(0); // remove first line
            this.SetAbsenderTelefonNr(this.ExtractSingleLineContent("Telefon"));
            this.SetAlarmzeit(this.ExtractSingleLineContent("Zeit"));
            this.ExtractSingleLineContent("Termin"); // remove line with Termin
            this.SetEinsatznr(this.ExtractSingleLineContent("Einsatzn"));
            this.ExtractSingleLineContent("EINSATZORT", Delete.AllBefore, false);
            this.SetStrasse(this.ExtractSingleLineContent("Straße"));
            this.SetHausnr(this.ExtractSingleLineContent("Haus-Nr", Delete.AllBefore));
            while (this.Content[counter].IndexOf("Ortsteil") < 0 && this.Content[counter] != null)
            {
                this.SetAbschnitt(this.Content[counter]);
                counter++;
            }
            this.SetOrt1(this.Content[counter]);
            this.SetPlz(this.Content[counter + 1]);
            counter += 2;
            if (this.Content[counter].IndexOf("Koordinate") >= 0)
            {
                this.SetKoordinate(this.Content[counter]);
                counter += 1;
            }
            this.SetObjekt(this.Content[counter]);
            counter += 3;
            this.SetSchlagwort(this.Content[counter]);
            counter += 1;
            this.SetStichwort(this.Content[counter], this.Content[counter + 1], this.Content[counter + 2], this.Content[counter + 3], this.Content[counter + 4]);
            counter += 5;
            while (this.Content[counter].IndexOf("EINSATZMITTEL") < 0)
            {
                counter++;
            }
            while (counter < this.Content.Count && this.Content[counter].IndexOf("BEMERKUNG") < 0)
            {
                this.SetEinsatzmittel1(this.Content[counter]);
                counter++;
            }
            if (this.Content.Count() <= counter)
            {
                // no line with BEMERKUNG detected -> go back until you find line with "Ausstattung"
                int merker = counter;
                counter--;
                while (counter > 0 && this.Content[counter].IndexOf("Ausstattung") < 0)
                {
                    counter--;
                }
                if (counter == 0)
                {
                    // no line with "Ausstattung" detected -> go to the end of the file
                    counter = merker;
                }
            }
            counter++;
            while (counter < this.Content.Count && this.Content[counter] != null && this.Content[counter].IndexOf("TEXTBAUSTEIN") < 0)
            {
                this.SetBemerkung(this.Content[counter]);
                counter++;
            }

        }

        public void SetEinsatznr(string raw)
        {
            raw = raw.Replace("Einsatzn", "");
            raw = raw.Trim(CharsToTrim);
            EinsatzNr = raw;
        }


    }

    /// <summary>
    /// Class for Alarmfaxinhalt ILS Wuerzburg
    /// </summary>
    class AlarmfaxinhaltIlsWue : Alarmfaxinhalt
    {
        public AlarmfaxinhaltIlsWue(List<string> content, string absender)
        {
            this.Content = content;
            this.Absender = absender;
        }

        public void ParseContent()
        {
            int counter = 0;
            this.Content.RemoveAt(0); // remove first line
            this.SetAlarmzeit(this.ExtractSingleLineContent("Termin"));
            this.SetEinsatznr(this.ExtractSingleLineContent("Einsatz"));
            this.ExtractSingleLineContent("EINSATZORT", Delete.AllBefore, false);
            this.SetStrasse(this.Content[counter]);
            this.SetHausnr(this.Content[counter]);
            this.SetAbschnitt(this.ExtractSingleLineContent("Abschnitt", Delete.AllBefore, true, true));
            this.SetOrt2(this.ExtractSingleLineContent("Ort", Delete.AllBefore, true, false));
            this.SetKoordinate(this.ExtractSingleLineContent("Koordinate", Delete.AllBefore, true, true));
            this.SetSchlagwort(this.ExtractSingleLineContent("Schlagw", Delete.AllBefore, true, false));
            this.SetWithEquipment(this.ExtractSingleLineContent("Name", Delete.AllBefore, true, false));
            this.SetBemerkung(this.ExtractMultiLineContent("BEMERKUNG", "TEXTBAUSTEIN", Delete.Single, false, false, false, true));
            while (counter < this.Content.Count)
            {
                this.SetEinsatzmittel2(this.Content[counter]);
                counter++;
            }
        }

        public void SetEinsatznr(string raw)
        {
            raw = raw.Replace("Einsatznummer", "");
            raw = raw.Replace("Einsatz", "");
            raw = raw.Trim(CharsToTrim);
            EinsatzNr = raw;
        }
    }

    /// <summary>
    /// Class for Alarmfaxinhalt ILS Amberg
    /// </summary>
    class AlarmfaxinhaltIlsAm : Alarmfaxinhalt
    {
        public AlarmfaxinhaltIlsAm(List<string> content, string absender)
        {
            this.Content = content;
            this.Absender = absender;
        }

        public void ParseContent()
        {
            this.Content.RemoveAt(0); // remove first line
            this.SetEinsatznr(this.ExtractSingleLineContent("Einsatznummer", Delete.AllBefore));
            this.KoordinateX = this.ExtractSingleLineContent("X", Delete.AllBefore, true, true);
            this.KoordinateY = this.ExtractSingleLineContent("y", Delete.AllBefore, true, true);
            this.Strasse = this.ExtractSingleLineContent("Straße", Delete.AllBefore, true, true);
            this.Hausnr = this.ExtractSingleLineContent("Haus-Nr", Delete.AllBefore, true, true);
            this.SetOrt2(this.ExtractSingleLineContent("Ort", Delete.AllBefore));
            this.Objekt = this.ExtractSingleLineContent("Objekt", Delete.AllBefore, true, true);
            this.Schlagwort = this.ExtractSingleLineContent("Schlagw", Delete.AllBefore, true, true);
            this.Stichwort = this.ExtractSingleLineContent("Stichwort", Delete.AllBefore, true, true);
            this.Bemerkung = this.ExtractMultiLineContent("BEMERKUNGEN", "EINSATZMITTEL", Delete.AllBefore);
            int counter = 0;
            while (counter < this.Content.Count && this.Content[counter].IndexOf("ENDE ALARMFAX") < 0)
            {
                this.SetEinsatzmittel1(this.Content[counter]);
                counter++;
            }
            
        }

        private void SetEinsatznr(string raw)
        {
            raw = raw.Replace("Einsatznummer", "");
            raw = raw.Trim(CharsToTrim);
            EinsatzNr = raw;
        }
    }

    /// <summary>
    /// Class for Alarmfaxinhalt ILS Traunstein
    /// </summary>
    class AlarmfaxinhaltIlsTs : Alarmfaxinhalt
    {
        public AlarmfaxinhaltIlsTs(List<string> content, string absender)
        {
            this.Content = content;
            this.Absender = absender;
        }

        public void ParseContent()
        {
            this.Content.RemoveAt(0); // remove first line
            string[] stringList1 = ExtractSingleLineMultiContent(new List<string> { "Einsatznummer", "Alarmzeit" }, Delete.AllBefore);
            this.SetEinsatznr(stringList1[0]);
            this.SetAlarmzeit(stringList1[1]);
            stringList1 = ExtractSingleLineMultiContent(new List<string> { "Straße", "Haus-Nr" }, Delete.AllBefore);
            this.Strasse = stringList1[0].Trim(CharsToTrim);
            this.Hausnr = stringList1[1].Trim(CharsToTrim);
            this.Abschnitt = ExtractSingleLineContent("Abschnitt", Delete.AllBefore, true, true);
            this.SetOrt2(this.ExtractSingleLineContent("Ort", Delete.AllBefore));
            this.Schlagwort = this.ExtractSingleLineContent("Schlagw", Delete.AllBefore, true, true);
            stringList1 = ExtractSingleLineMultiContent(new List<string> { "Stichwort B", "Stichwort RD" }, Delete.AllBefore);
            string[] stringList2 = ExtractSingleLineMultiContent(new List<string> { "Stichwort SO", "Stichwort TH", "Stichwort IN" }, Delete.AllBefore);
            this.SetStichwort(stringList1[0], stringList2[1], stringList2[0], stringList2[2], stringList1[1]);
            ExtractSingleLineContent("EINSATZMITTEL", Delete.AllBefore, false);

            int counter = 0;
            while (counter < this.Content.Count && this.Content[counter].IndexOf("BEMERKUNG") < 0)
            {
                this.SetEinsatzmittel1(this.Content[counter]);
                counter++;
            }

            this.Bemerkung = ExtractMultiLineContent("BEMERKUNG", "EINSATZKOORDINATEN", Delete.AllBefore, false, false, false);
            this.KoordinateX = this.ExtractSingleLineContent("Rechtswert", Delete.AllBefore, true, true);
            this.KoordinateY = this.ExtractSingleLineContent("Hochwert", Delete.AllBefore, true, true);
        }

        private void SetEinsatznr(string raw)
        {
            raw = raw.Replace("Einsatznummer", "");
            raw = raw.Trim(CharsToTrim);
            EinsatzNr = raw;
        }

    }
}
