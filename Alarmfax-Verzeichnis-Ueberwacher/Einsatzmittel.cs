﻿/*
    Copyright (C) 2020 Ralf Weippert - All Rights Reserved    

    This file is part of Alarmfax-Verzeichnis-Überwacher.

    Alarmfax-Verzeichnis-Überwacher is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Alarmfax-Verzeichnis-Überwacher is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Alarmfax-Verzeichnis-Überwacher.  If not, see <http://www.gnu.org/licenses/>.

    Diese Datei ist Teil von Alarmfax-Verzeichnis-Überwacher.

    Das Programm Alarmfax-Verzeichnis-Überwacher ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Das Programm Alarmfax-Verzeichnis-Überwacher wird in der Hoffnung, dass es nützlich sein wird, aber
    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Details.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    class Einsatzmittel
    {
        private string County = "";
        private string DepartmentType = "";
        private string Name = "";
        private string RadioIndex = "";
        private string Equipment = "";
        private string DispatchTime = "";
        private readonly char[] CharsToTrim = { '.', ' ', ':', '=' };

        public Einsatzmittel(string rawName)
        {
            rawName = rawName.Replace("Einsatzmittelname", "");
            rawName = rawName.Replace("Name", "");
            rawName = rawName.Trim(CharsToTrim);
            
            if (rawName.IndexOf("Fehlt") >= 0)
            {
                Name = rawName.Trim(CharsToTrim);
                return;
            }
            Regex r = new Regex(@"^\d");

            if (r.Match(rawName).Success) 
            {
                int firstBlank, secondBlank, lastBlank;
                lastBlank = rawName.LastIndexOf(" ");
                firstBlank = rawName.IndexOf(" ");
                secondBlank = rawName.IndexOf(" ", firstBlank + 1);
                County = rawName.Substring(firstBlank + 1, secondBlank - firstBlank - 1);

                firstBlank = secondBlank;
                secondBlank = rawName.IndexOf(" ", firstBlank + 1);
                DepartmentType = rawName.Substring(firstBlank + 1, secondBlank - firstBlank - 1);

                if (lastBlank == secondBlank)
                {
                    // no radio index
                    Name = rawName.Substring(secondBlank + 1);
                }
                else
                {
                    // radio index
                    Name = rawName.Substring(secondBlank + 1, lastBlank - secondBlank - 1);
                    RadioIndex = rawName.Substring(lastBlank + 1);
                }
            }
            else
            {
                int lastBlank;
                lastBlank = rawName.LastIndexOf(" ");
                string lastPart = rawName.Substring(lastBlank+1);
                if (r.Match(lastPart).Success)
                {
                    // radio index
                    Name = rawName.Substring(0, lastBlank).Trim();
                    RadioIndex = lastPart.Trim();
                }
                else
                {
                    // no radio index
                    Name = rawName;
                }
            }

        }
        public void AddEquipment(string raw)
        {
            Equipment = raw.Replace("Ausstattung", "");
            Equipment = Equipment.Replace("gef. Geräte", "");
            Equipment = Equipment.Replace("Gerät :", "");
            Equipment = Equipment.Replace("Gerät:", "");
            Equipment = Equipment.Trim(CharsToTrim);
        }
        public void AddDispatchTime(string raw)
        {
            DispatchTime = raw.Replace("Alarmiert", "");
            DispatchTime = DispatchTime.Trim(CharsToTrim);
        }
        public string GetEquipment(bool startBlank)
        {
            if (Equipment != "")
            {
                if (startBlank)
                {
                    return " " + Equipment;
                }
                else
                {
                    return Equipment;
                }
            }
            else
            {
                return "";
            }
        }
        public string GetCounty()
        {
            return County;
        }
        public string GetDepartmentType(bool endBlank)
        {
            if (DepartmentType != "")
            {
                if (endBlank)
                {
                    return DepartmentType + " ";
                }
                else
                {
                    return DepartmentType;
                }
            }
            else
            {
                return "";
            }
        }
        public string GetName(bool startBlank)
        {
            if (startBlank)
            {
                return " " + Name;
            }
            else
            {
                return Name;
            }
        }
        public string GetRadioIndex(bool startBlank)
        {
            if (RadioIndex != "")
            {
                if (startBlank)
                {
                    return " " + RadioIndex;
                }
                else
                {
                    return RadioIndex;
                }
            }
            else
            {
                return "";
            }
        }
    }
}
