﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Tests
{
    [TestClass()]
    public class AlarmfaxinhaltTests
    {
        [TestMethod()]
        public void setAbsenderTest1()
        {
            string raw = "Absender: Testabsender";
            string expected = "<Absender>Testabsender</Absender>";
            var af = new Alarmfaxinhalt();
            af.SetAbsender(raw);
            string actual = af.GetAbsender();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAbsenderTest2()
        {
            string raw = "Testabsender";
            string expected = "<Absender>Testabsender</Absender>";
            var af = new Alarmfaxinhalt();
            af.SetAbsender(raw);
            string actual = af.GetAbsender();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAbsenderTest3()
        {
            string raw = "Absender: =: Testabsender . . =: ";
            string expected = "<Absender>Testabsender</Absender>";
            var af = new Alarmfaxinhalt();
            af.SetAbsender(raw);
            string actual = af.GetAbsender();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAbsenderTest4()
        {
            string raw = "";
            string expected = "<Absender></Absender>";
            var af = new Alarmfaxinhalt();
            af.SetAbsender(raw);
            string actual = af.GetAbsender();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAbsenderTest5()
        {
            string raw = null;
            string expected = "<Absender></Absender>";
            var af = new Alarmfaxinhalt();
            af.SetAbsender(raw);
            string actual = af.GetAbsender();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAlarmzeitTest1()
        {
            string raw = null;
            string expected = "<Alarmzeit></Alarmzeit>";
            var af = new Alarmfaxinhalt();
            af.SetAlarmzeit(raw);
            string actual = af.GetAlarmzeit();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAlarmzeitTest2()
        {
            string raw = "Zeit: a";
            string expected = "<Alarmzeit>a</Alarmzeit>";
            var af = new Alarmfaxinhalt();
            af.SetAlarmzeit(raw);
            string actual = af.GetAlarmzeit();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAlarmzeitTest3()
        {
            string raw = "Termin: a";
            string expected = "<Alarmzeit>a</Alarmzeit>";
            var af = new Alarmfaxinhalt();
            af.SetAlarmzeit(raw);
            string actual = af.GetAlarmzeit();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAlarmzeitTest4()
        {
            string raw = "Zeit:Termin: a";
            string expected = "<Alarmzeit>a</Alarmzeit>";
            var af = new Alarmfaxinhalt();
            af.SetAlarmzeit(raw);
            string actual = af.GetAlarmzeit();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAlarmzeitTest5()
        {
            string raw = "";
            string expected = "<Alarmzeit></Alarmzeit>";
            var af = new Alarmfaxinhalt();
            af.SetAlarmzeit(raw);
            string actual = af.GetAlarmzeit();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void setAbsenderFaxNrTest1()
        {
            Assert.Fail();
        }
    }
}